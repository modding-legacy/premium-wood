package com.legacy.premium_wood.block;

import com.mojang.serialization.MapCodec;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class PremiumBookshelfBlock extends Block
{
	public static final MapCodec<PremiumBookshelfBlock> CODEC = simpleCodec(PremiumBookshelfBlock::new);

	public PremiumBookshelfBlock(BlockBehaviour.Properties props)
	{
		super(props);
	}

	@Override
	protected MapCodec<? extends PremiumBookshelfBlock> codec()
	{
		return CODEC;
	}
}
