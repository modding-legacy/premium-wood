package com.legacy.premium_wood.block;

import com.legacy.premium_wood.client.container.PremiumWorkbenchContainer;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.inventory.ContainerLevelAccess;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.CraftingTableBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;

public class PremiumWorkbenchBlock extends CraftingTableBlock
{
	public static final MapCodec<PremiumWorkbenchBlock> CODEC = simpleCodec(PremiumWorkbenchBlock::new);
	private static final Component GUI_TITLE = Component.translatable("container.crafting");

	public PremiumWorkbenchBlock(BlockBehaviour.Properties props)
	{
		super(props);
	}

	@Override
	public MapCodec<? extends CraftingTableBlock> codec()
	{
		return CODEC;
	}

	@Override
	public MenuProvider getMenuProvider(BlockState state, Level worldIn, BlockPos pos)
	{
		return new SimpleMenuProvider((id, inventory, entity) ->
		{
			return new PremiumWorkbenchContainer(id, inventory, ContainerLevelAccess.create(worldIn, pos), this);
		}, GUI_TITLE);
	}
}