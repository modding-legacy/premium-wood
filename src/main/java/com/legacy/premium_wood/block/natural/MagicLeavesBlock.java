package com.legacy.premium_wood.block.natural;

import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class MagicLeavesBlock extends LeavesBlock
{
	public static final MapCodec<MagicLeavesBlock> CODEC = simpleCodec(MagicLeavesBlock::new);

	public MagicLeavesBlock(Properties properties)
	{
		super(properties);
	}

	@Override
	public MapCodec<? extends LeavesBlock> codec()
	{
		return CODEC;
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, Level worldIn, BlockPos pos, RandomSource rand)
	{
		super.animateTick(stateIn, worldIn, pos, rand);

		/*float width = 1.2F;
		
		for (int i = 0; i < 2; ++i)
			worldIn.addParticle(ParticleTypes.RAIN, pos.getX() - 0.1F + rand.nextDouble() * width, pos.getY() - 0.1F + rand.nextDouble() * width, pos.getZ() - 0.1F + rand.nextDouble() * width, 0.0D, 0.0D, 0.0D);*/
	}
}
