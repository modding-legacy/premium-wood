package com.legacy.premium_wood.block.natural;

import com.legacy.premium_wood.PremiumConfig;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.grower.TreeGrower;
import net.minecraft.world.level.block.state.BlockState;

public class ExplodingSaplingBlock extends SaplingBlock
{
	public static final MapCodec<ExplodingSaplingBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(TreeGrower.CODEC.fieldOf("tree").forGetter(sapling -> sapling.treeGrower), propertiesCodec()).apply(instance, ExplodingSaplingBlock::new));

	public ExplodingSaplingBlock(TreeGrower grower, Properties properties)
	{
		super(grower, properties);
	}

	@Override
	public MapCodec<? extends SaplingBlock> codec()
	{
		return super.codec();
	}

	@Override
	public void performBonemeal(ServerLevel level, RandomSource rand, BlockPos pos, BlockState state)
	{
		if (level.isClientSide)
			return;

		if (rand.nextFloat() < ((float) PremiumConfig.COMMON.magicSaplingExplosionChance() / 100F))
		{
			level.removeBlock(pos, false);
			level.getLevel().explode(null, pos.getX(), pos.getY(), pos.getZ(), 2.0F, Level.ExplosionInteraction.NONE);
		}
		else
			super.performBonemeal(level, rand, pos, state);
	}
}
