package com.legacy.premium_wood.block.natural;

import com.legacy.premium_wood.PremiumConfig;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;

public class AppleLeavesBlock extends LeavesBlock
{
	public static final MapCodec<AppleLeavesBlock> CODEC = simpleCodec(AppleLeavesBlock::new);
	
	public static final BooleanProperty MATURE = BooleanProperty.create("mature");
	public static final BooleanProperty CAN_MATURE = BooleanProperty.create("can_mature");
	public static final BooleanProperty GOLDEN = BooleanProperty.create("golden");

	public AppleLeavesBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(super.defaultBlockState().setValue(MATURE, false).setValue(CAN_MATURE, false).setValue(GOLDEN, false));
	}

	@Override
	public MapCodec<? extends LeavesBlock> codec()
	{
		return CODEC;
	}
	
	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(MATURE, CAN_MATURE, GOLDEN);
	}

	@Override
	public boolean isRandomlyTicking(BlockState state)
	{
		return super.isRandomlyTicking(state) || state.getValue(CAN_MATURE) && !state.getValue(MATURE).booleanValue();
	}

	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		super.randomTick(state, level, pos, random);

		if (!level.isClientSide && state.getValue(CAN_MATURE).booleanValue() == true && state.getValue(MATURE).booleanValue() == false && random.nextInt(PremiumConfig.COMMON.appleMatureChance()) == 0)
			level.setBlockAndUpdate(pos, state.setValue(MATURE, true).setValue(GOLDEN, random.nextInt(4096) == 0));
	}

	@Override
	public InteractionResult useWithoutItem(BlockState state, Level level, BlockPos pos, Player player, BlockHitResult hit)
	{
		if (state.getValue(MATURE))
		{
			BlockPos offsetPos = pos.relative(hit.getDirection());
			level.addFreshEntity(new ItemEntity(level, offsetPos.getX() + 0.5F, offsetPos.getY() + 0.5F, offsetPos.getZ() + 0.5F, new ItemStack(state.getValue(GOLDEN) ? Items.GOLDEN_APPLE : Items.APPLE)));
			level.playSound((Player) null, pos, SoundEvents.GRASS_HIT, SoundSource.BLOCKS, 1.0F, 1.0F);
			level.playSound((Player) null, pos, SoundEvents.ITEM_PICKUP, SoundSource.BLOCKS, 0.5F, 0.5F);
			level.setBlockAndUpdate(pos, state.setValue(MATURE, false).setValue(GOLDEN, false));
			return InteractionResult.SUCCESS;
		}

		return InteractionResult.PASS;
	}
}
