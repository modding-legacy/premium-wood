package com.legacy.premium_wood.block.natural;

import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ScheduledTickAccess;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.IntegerProperty;

public class WillowLeavesBlock extends LeavesBlock
{
	public static final MapCodec<WillowLeavesBlock> CODEC = simpleCodec(WillowLeavesBlock::new);

	public static final IntegerProperty WILLOW_DISTANCE = IntegerProperty.create("willow_distance", 1, 13);
	public final static int maxWillowDistance = 13;

	public WillowLeavesBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(super.defaultBlockState().setValue(WILLOW_DISTANCE, Integer.valueOf(maxWillowDistance)));
	}

	@Override
	public MapCodec<? extends LeavesBlock> codec()
	{
		return CODEC;
	}

	@Override
	public boolean isRandomlyTicking(BlockState state)
	{
		return state.getValue(DISTANCE) == 7 && state.getValue(WILLOW_DISTANCE) == maxWillowDistance && !state.getValue(PERSISTENT);
	}

	@Override
	public void randomTick(BlockState state, ServerLevel worldIn, BlockPos pos, RandomSource random)
	{
		if (!state.getValue(PERSISTENT) && state.getValue(WILLOW_DISTANCE) == maxWillowDistance && state.getValue(DISTANCE) == 7)
		{
			dropResources(state, worldIn, pos);
			worldIn.removeBlock(pos, false);

			/*System.out.println(state.get(WILLOW_DISTANCE) + " " + state.get(DISTANCE));*/
		}
	}

	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, RandomSource rand)
	{
		worldIn.setBlock(pos, updateWillowDistance(state, worldIn, pos), 3);
	}

	/*BlockState p_54440_,
	LevelReader p_374064_,
	ScheduledTickAccess p_374538_, BlockPos p_54444_,
	Direction p_54441_, BlockPos p_54445_,
	BlockState p_54442_, RandomSource p_374122_*/

	@Override
	public BlockState updateShape(BlockState stateIn, LevelReader worldIn, ScheduledTickAccess tickAccess, BlockPos currentPos, Direction facing, BlockPos facingPos, BlockState facingState, RandomSource rand)
	{
		int i = getWillowDistance(facingState) + 1;
		if (i != 1 || stateIn.getValue(WILLOW_DISTANCE) != i)
		{
			tickAccess.scheduleTick(currentPos, this, 1);
		}

		return super.updateShape(stateIn, worldIn, tickAccess, currentPos, facing, facingPos, facingState, rand);
	}

	private static BlockState updateWillowDistance(BlockState state, LevelAccessor worldIn, BlockPos pos)
	{
		int willowDistance = maxWillowDistance;
		BlockPos.MutableBlockPos blockpos$mutable = new BlockPos.MutableBlockPos();

		for (Direction direction : Direction.values())
		{
			blockpos$mutable.setWithOffset(pos, direction);
			willowDistance = Math.min(willowDistance, getWillowDistance(worldIn.getBlockState(blockpos$mutable)) + 1);
			if (willowDistance == 1)
			{
				break;
			}
		}

		int distance = 7;
		for (Direction direction : Direction.values())
		{
			blockpos$mutable.setWithOffset(pos, direction);
			distance = Math.min(distance, getDistance(worldIn.getBlockState(blockpos$mutable)) + 1);
			if (distance == 1)
			{
				break;
			}
		}

		return state.setValue(WILLOW_DISTANCE, Integer.valueOf(willowDistance)).setValue(DISTANCE, Integer.valueOf(distance));
	}

	private static int getWillowDistance(BlockState neighbor)
	{
		if (neighbor.is(BlockTags.LOGS))
		{
			return 0;
		}
		else
		{
			return neighbor.getBlock() instanceof WillowLeavesBlock ? neighbor.getValue(WILLOW_DISTANCE) : maxWillowDistance;
		}
	}

	private static int getDistance(BlockState neighbor)
	{
		if (neighbor.is(BlockTags.LOGS))
		{
			return 0;
		}
		else
		{
			return neighbor.getBlock() instanceof LeavesBlock ? neighbor.getValue(DISTANCE) : 7;
		}
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(WILLOW_DISTANCE);
	}
}
