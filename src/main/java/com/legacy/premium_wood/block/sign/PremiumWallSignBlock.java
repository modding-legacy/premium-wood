package com.legacy.premium_wood.block.sign;

import com.legacy.premium_wood.block_entity.PremiumSignBlockEntity;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.SignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;

public class PremiumWallSignBlock extends WallSignBlock
{
	public static final MapCodec<WallSignBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(WoodType.CODEC.fieldOf("wood_type").forGetter(SignBlock::type), propertiesCodec()).apply(instance, PremiumWallSignBlock::new));

	public PremiumWallSignBlock(WoodType type, Properties properties)
	{
		super(type, properties);
	}

	@Override
	public MapCodec<WallSignBlock> codec()
	{
		return CODEC;
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new PremiumSignBlockEntity(pos, state);
	}
}
