package com.legacy.premium_wood.block.sign;

import com.legacy.premium_wood.block_entity.PremiumSignBlockEntity;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.SignBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;

public class PremiumStandingSignBlock extends StandingSignBlock
{
	public static final MapCodec<StandingSignBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(WoodType.CODEC.fieldOf("wood_type").forGetter(SignBlock::type), propertiesCodec()).apply(instance, PremiumStandingSignBlock::new));

	public PremiumStandingSignBlock(WoodType type, Properties properties)
	{
		super(type, properties);
	}

	@Override
	public MapCodec<StandingSignBlock> codec()
	{
		return CODEC;
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new PremiumSignBlockEntity(pos, state);
	}
}
