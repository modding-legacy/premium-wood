package com.legacy.premium_wood.block.sign;

import com.legacy.premium_wood.block_entity.PremiumHangingSignBlockEntity;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.SignBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;

public class PremiumWallHangingSignBlock extends WallHangingSignBlock
{
	public static final MapCodec<WallHangingSignBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(WoodType.CODEC.fieldOf("wood_type").forGetter(SignBlock::type), propertiesCodec()).apply(instance, PremiumWallHangingSignBlock::new));

	public PremiumWallHangingSignBlock(WoodType type, Properties properties)
	{
		super(type, properties);
	}

	@Override
	public MapCodec<WallHangingSignBlock> codec()
	{
		return CODEC;
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new PremiumHangingSignBlockEntity(pos, state);
	}
}
