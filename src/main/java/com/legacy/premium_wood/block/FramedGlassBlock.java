package com.legacy.premium_wood.block;

import com.mojang.serialization.MapCodec;

import net.minecraft.world.level.block.TransparentBlock;

public class FramedGlassBlock extends TransparentBlock
{
	public static final MapCodec<FramedGlassBlock> CODEC = simpleCodec(FramedGlassBlock::new);
	
	public FramedGlassBlock(Properties properties)
	{
		super(properties);
	}
	
	@Override
	protected MapCodec<? extends TransparentBlock> codec()
	{
		return CODEC;
	}
}