package com.legacy.premium_wood.block;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class PremiumFlowerPotBlock extends FlowerPotBlock
{
	public static final MapCodec<FlowerPotBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(BuiltInRegistries.BLOCK.byNameCodec().fieldOf("potted").forGetter(pot -> pot.getPotted()), propertiesCodec()).apply(instance, PremiumFlowerPotBlock::new));

	@SuppressWarnings("deprecation")
	private PremiumFlowerPotBlock(Block flower, BlockBehaviour.Properties properties)
	{
		super(flower, properties);
	}

	@Override
	public MapCodec<FlowerPotBlock> codec()
	{
		return CODEC;
	}

	public PremiumFlowerPotBlock(@Nullable Supplier<FlowerPotBlock> emptyPot, Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		super(emptyPot, flower, properties);
		((FlowerPotBlock) Blocks.FLOWER_POT).addPlant(BuiltInRegistries.BLOCK.getKey(flower.get()), () -> this);
	}

	public PremiumFlowerPotBlock(Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		this(() -> (FlowerPotBlock) Blocks.FLOWER_POT, flower, properties);
	}
}
