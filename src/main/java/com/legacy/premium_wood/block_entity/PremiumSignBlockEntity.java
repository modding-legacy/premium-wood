package com.legacy.premium_wood.block_entity;

import com.legacy.premium_wood.registry.PWBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class PremiumSignBlockEntity extends SignBlockEntity
{
	public PremiumSignBlockEntity(BlockPos pos, BlockState state)
	{
		super(pos, state);
	}

	@Override
	public BlockEntityType<?> getType()
	{
		return PWBlockEntityTypes.PREMIUM_SIGN.get();
	}
}