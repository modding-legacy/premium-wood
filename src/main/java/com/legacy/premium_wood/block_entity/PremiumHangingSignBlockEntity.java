package com.legacy.premium_wood.block_entity;

import com.legacy.premium_wood.registry.PWBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.HangingSignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class PremiumHangingSignBlockEntity extends HangingSignBlockEntity
{
	public PremiumHangingSignBlockEntity(BlockPos pos, BlockState state)
	{
		super(pos, state);
	}

	@Override
	public BlockEntityType<PremiumHangingSignBlockEntity> getType()
	{
		return PWBlockEntityTypes.PREMIUM_HANGING_SIGN.get();
	}
}
