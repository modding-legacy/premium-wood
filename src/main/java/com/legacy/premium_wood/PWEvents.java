package com.legacy.premium_wood;

import com.legacy.premium_wood.item.ToolCompat;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.event.level.BlockEvent.BlockToolModificationEvent;

public class PWEvents
{
	@SubscribeEvent
	public static void onToolUse(final BlockToolModificationEvent event)
	{
		var toolAction = event.getItemAbility();
		if (toolAction == ItemAbilities.AXE_STRIP)
		{
			BlockState finalState = event.getFinalState();
			Block result = ToolCompat.AXE_STRIPPING.get(finalState.getBlock());
			if (result != null)
				event.setFinalState(result.withPropertiesOf(finalState));
		}
	}
}
