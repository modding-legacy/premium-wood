package com.legacy.premium_wood;

import com.legacy.premium_wood.block.PremiumBookshelfBlock;
import com.legacy.premium_wood.block.PremiumWorkbenchBlock;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.furnace.FurnaceFuelBurnTimeEvent;

public class FuelHandler
{
	@SubscribeEvent
	public static void getBurnTime(FurnaceFuelBurnTimeEvent event)
	{
		Item item = event.getItemStack().getItem();
		var key = BuiltInRegistries.ITEM.getKey(item);

		if (key != null && key.getNamespace().equals(PremiumWoodMod.MODID))
			event.setBurnTime(getBurnTime(item));
	}

	private static int getBurnTime(Item item)
	{
		if (item instanceof BlockItem blockItem)
		{
			Block block = blockItem.getBlock();
			if (block instanceof FenceGateBlock || block instanceof FenceBlock || block instanceof PremiumWorkbenchBlock || block instanceof PremiumBookshelfBlock)
				return 300;
		}
		/*else if (item instanceof DiggerItem && ((DiggerItem) item).getTier() == PremiumItemTiers.MAGIC)
			return 200;*/

		return -1;
	}
}
