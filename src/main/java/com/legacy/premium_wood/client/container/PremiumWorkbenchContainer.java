package com.legacy.premium_wood.client.container;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.CraftingMenu;
import net.minecraft.world.inventory.ContainerLevelAccess;

public class PremiumWorkbenchContainer extends CraftingMenu
{
	private final Block workbench;
	private ContainerLevelAccess worldPos;

	public PremiumWorkbenchContainer(int id, Inventory playerInv, ContainerLevelAccess worldPos, Block workbench)
	{
		super(id, playerInv, worldPos);
		this.workbench = workbench;
		this.worldPos = worldPos;
	}

	@Override
	public boolean stillValid(Player playerIn)
	{
		return isWithinUsableDistance(this.worldPos, playerIn, this.workbench);
	}

	protected static boolean isWithinUsableDistance(ContainerLevelAccess worldPos, Player playerIn, Block targetBlock)
	{
		return worldPos.evaluate((world, pos) ->
		{
			return world.getBlockState(pos).getBlock() != targetBlock ? false : playerIn.distanceToSqr((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D) <= 64.0D;
		}, true);
	}
}