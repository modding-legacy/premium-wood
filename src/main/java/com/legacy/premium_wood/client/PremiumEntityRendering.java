package com.legacy.premium_wood.client;

import java.util.function.Supplier;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.registry.PWBlockEntityTypes;
import com.legacy.premium_wood.registry.PWEntityTypes;

import net.minecraft.client.model.BoatModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.renderer.blockentity.HangingSignRenderer;
import net.minecraft.client.renderer.blockentity.SignRenderer;
import net.minecraft.client.renderer.entity.BoatRenderer;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

public class PremiumEntityRendering
{
	public static final ModelLayerLocation MAPLE_BOAT = boatLayer("maple", false);
	public static final ModelLayerLocation MAPLE_CHEST_BOAT = boatLayer("maple", true);

	public static final ModelLayerLocation TIGER_BOAT = boatLayer("tiger", false);
	public static final ModelLayerLocation TIGER_CHEST_BOAT = boatLayer("tiger", true);

	public static final ModelLayerLocation SILVERBELL_BOAT = boatLayer("silverbell", false);
	public static final ModelLayerLocation SILVERBELL_CHEST_BOAT = boatLayer("silverbell", true);

	public static final ModelLayerLocation PURPLE_HEART_BOAT = boatLayer("purple_heart", false);
	public static final ModelLayerLocation PURPLE_HEART_CHEST_BOAT = boatLayer("purple_heart", true);

	public static final ModelLayerLocation WILLOW_BOAT = boatLayer("willow", false);
	public static final ModelLayerLocation WILLOW_CHEST_BOAT = boatLayer("willow", true);

	public static final ModelLayerLocation MAGIC_BOAT = boatLayer("magic", false);
	public static final ModelLayerLocation MAGIC_CHEST_BOAT = boatLayer("magic", true);

	public static void init(IEventBus modBus)
	{
		modBus.addListener(PremiumEntityRendering::initLayers);
		modBus.addListener(PremiumEntityRendering::initRenders);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		Supplier<LayerDefinition> boat = () -> BoatModel.createBoatModel();
		Supplier<LayerDefinition> chestBoat = () -> BoatModel.createChestBoatModel();

		event.registerLayerDefinition(MAPLE_BOAT, boat);
		event.registerLayerDefinition(MAPLE_CHEST_BOAT, chestBoat);

		event.registerLayerDefinition(TIGER_BOAT, boat);
		event.registerLayerDefinition(TIGER_CHEST_BOAT, chestBoat);

		event.registerLayerDefinition(SILVERBELL_BOAT, boat);
		event.registerLayerDefinition(SILVERBELL_CHEST_BOAT, chestBoat);

		event.registerLayerDefinition(PURPLE_HEART_BOAT, boat);
		event.registerLayerDefinition(PURPLE_HEART_CHEST_BOAT, chestBoat);

		event.registerLayerDefinition(WILLOW_BOAT, boat);
		event.registerLayerDefinition(WILLOW_CHEST_BOAT, chestBoat);

		event.registerLayerDefinition(MAGIC_BOAT, boat);
		event.registerLayerDefinition(MAGIC_CHEST_BOAT, chestBoat);
	}

	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(PWEntityTypes.MAPLE_BOAT, c -> new BoatRenderer(c, MAPLE_BOAT));
		event.registerEntityRenderer(PWEntityTypes.MAPLE_CHEST_BOAT, c -> new BoatRenderer(c, MAPLE_CHEST_BOAT));

		event.registerEntityRenderer(PWEntityTypes.TIGER_BOAT, c -> new BoatRenderer(c, TIGER_BOAT));
		event.registerEntityRenderer(PWEntityTypes.TIGER_CHEST_BOAT, c -> new BoatRenderer(c, TIGER_CHEST_BOAT));

		event.registerEntityRenderer(PWEntityTypes.SILVERBELL_BOAT, c -> new BoatRenderer(c, SILVERBELL_BOAT));
		event.registerEntityRenderer(PWEntityTypes.SILVERBELL_CHEST_BOAT, c -> new BoatRenderer(c, SILVERBELL_CHEST_BOAT));

		event.registerEntityRenderer(PWEntityTypes.PURPLE_HEART_BOAT, c -> new BoatRenderer(c, PURPLE_HEART_BOAT));
		event.registerEntityRenderer(PWEntityTypes.PURPLE_HEART_CHEST_BOAT, c -> new BoatRenderer(c, PURPLE_HEART_CHEST_BOAT));

		event.registerEntityRenderer(PWEntityTypes.WILLOW_BOAT, c -> new BoatRenderer(c, WILLOW_BOAT));
		event.registerEntityRenderer(PWEntityTypes.WILLOW_CHEST_BOAT, c -> new BoatRenderer(c, WILLOW_CHEST_BOAT));

		event.registerEntityRenderer(PWEntityTypes.MAGIC_BOAT, c -> new BoatRenderer(c, MAGIC_BOAT));
		event.registerEntityRenderer(PWEntityTypes.MAGIC_CHEST_BOAT, c -> new BoatRenderer(c, MAGIC_CHEST_BOAT));

		event.registerBlockEntityRenderer(PWBlockEntityTypes.PREMIUM_SIGN.get(), SignRenderer::new);
		event.registerBlockEntityRenderer(PWBlockEntityTypes.PREMIUM_HANGING_SIGN.get(), HangingSignRenderer::new);
	}

	public static ModelLayerLocation boatLayer(String name, boolean chest)
	{
		return new ModelLayerLocation(PremiumWoodMod.locate((chest ? "chest_boat/" : "boat/") + name), "main");
	}
}