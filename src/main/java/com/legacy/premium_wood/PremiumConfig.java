package com.legacy.premium_wood;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.common.ModConfigSpec.ConfigValue;

public class PremiumConfig
{
	public static final ModConfigSpec COMMON_SPEC;
	public static final CommonConfig COMMON;

	static
	{
		{
			Pair<CommonConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(CommonConfig::new);
			COMMON = pair.getLeft();
			COMMON_SPEC = pair.getRight();
		}
	}

	public static class CommonConfig
	{
		private final ConfigValue<Integer> appleMatureChance;
		private final ConfigValue<Integer> magicSaplingExplosionChance;

		public CommonConfig(ModConfigSpec.Builder builder)
		{
			builder.comment("\n Premium Wood Common Config\n\n NOTICE: If you're looking for tree spawn options, those have been moved.\n Their new home is inside the mod's data folder, in the form of tags and/or biome modifiers.\n These can be modified using a custom datapack.\n");

			builder.push("Live World");
			appleMatureChance = builder.comment("Chance for a maturable apple leaf to mature every random tick once picked. (1/X)\n Default: 60").define("apple_mature_chance", 60);
			magicSaplingExplosionChance = builder.comment("Percent chance for the Magic Sapling to harmlessly explode instead of growing when being bonemealed. Setting this to 0 will disable this entirely.").defineInRange("magic_explosion_percentage", 10, 0, 100);
			builder.pop();
		}

		public int appleMatureChance()
		{
			return this.appleMatureChance.get();
		}
		
		public int magicSaplingExplosionChance()
		{
			return this.magicSaplingExplosionChance.get();
		}
	}
}