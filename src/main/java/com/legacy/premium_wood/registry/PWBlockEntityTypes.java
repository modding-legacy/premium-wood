package com.legacy.premium_wood.registry;

import java.util.Set;
import java.util.function.Supplier;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block_entity.PremiumHangingSignBlockEntity;
import com.legacy.premium_wood.block_entity.PremiumSignBlockEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PWBlockEntityTypes
{
	/*private static final Lazy<Block[]> SIGNS = Lazy.of(() -> ForgeRegistries.BLOCKS.getValues().stream().filter(block -> ForgeRegistries.BLOCKS.getKey(block).getNamespace().equals(PremiumWoodMod.MODID) && block instanceof SignBlock).toArray(Block[]::new));*/
	public static final Supplier<BlockEntityType<PremiumSignBlockEntity>> PREMIUM_SIGN = Lazy.of(() -> create(PremiumSignBlockEntity::new, PWBlocks.maple_sign, PWBlocks.maple_wall_sign, PWBlocks.tiger_sign, PWBlocks.tiger_wall_sign, PWBlocks.silverbell_sign, PWBlocks.silverbell_wall_sign, PWBlocks.purple_heart_sign, PWBlocks.purple_heart_wall_sign, PWBlocks.willow_sign, PWBlocks.willow_wall_sign, PWBlocks.magic_sign, PWBlocks.magic_wall_sign));
	public static final Supplier<BlockEntityType<PremiumHangingSignBlockEntity>> PREMIUM_HANGING_SIGN = Lazy.of(() -> create(PremiumHangingSignBlockEntity::new, PWBlocks.maple_hanging_sign, PWBlocks.maple_wall_hanging_sign, PWBlocks.tiger_hanging_sign, PWBlocks.tiger_wall_hanging_sign, PWBlocks.silverbell_hanging_sign, PWBlocks.silverbell_wall_hanging_sign, PWBlocks.purple_heart_hanging_sign, PWBlocks.purple_heart_wall_hanging_sign, PWBlocks.willow_hanging_sign, PWBlocks.willow_wall_hanging_sign, PWBlocks.magic_hanging_sign, PWBlocks.magic_wall_hanging_sign));

	public static void init(RegisterEvent event)
	{
		register(event, "premium_sign", PREMIUM_SIGN);
		register(event, "premium_hanging_sign", PREMIUM_HANGING_SIGN);
	}

	@SuppressWarnings("unchecked")
	private static <T extends BlockEntity> void register(RegisterEvent event, String name, Object blockEntity)
	{
		event.register(Registries.BLOCK_ENTITY_TYPE, PremiumWoodMod.locate(name), (Supplier<BlockEntityType<?>>) blockEntity);
	}

	private static <T extends BlockEntity> BlockEntityType<T> create(BlockEntityType.BlockEntitySupplier<? extends T> p_362578_, Block... p_364748_)
	{
		return new BlockEntityType<>(p_362578_, Set.of(p_364748_));
	}
}
