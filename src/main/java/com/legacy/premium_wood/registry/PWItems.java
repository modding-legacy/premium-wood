package com.legacy.premium_wood.registry;

import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.legacy.premium_wood.PremiumWoodMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.BoatItem;
import net.minecraft.world.item.HangingSignItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SignItem;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PWItems
{
	public static Item maple_boat, tiger_boat, silverbell_boat, purple_heart_boat, willow_boat, magic_boat;
	public static Item maple_chest_boat, tiger_chest_boat, silverbell_chest_boat, purple_heart_chest_boat,
			willow_chest_boat, magic_chest_boat;

	public static Item maple_sign, tiger_sign, silverbell_sign, purple_heart_sign, willow_sign, magic_sign;
	public static Item maple_hanging_sign, tiger_hanging_sign, silverbell_hanging_sign, purple_heart_hanging_sign,
			willow_hanging_sign, magic_hanging_sign;

	private static RegisterEvent registryEvent;

	public static void init(RegisterEvent event)
	{
		PWItems.registryEvent = event;
		registerBlockItems();

		Supplier<Item.Properties> boatProps = () -> new Item.Properties().stacksTo(1);
		Supplier<Item.Properties> signProps = () -> new Item.Properties().stacksTo(16).useBlockDescriptionPrefix();

		maple_boat = register("maple_boat", p -> new BoatItem(PWEntityTypes.MAPLE_BOAT, p), boatProps);
		maple_chest_boat = register("maple_chest_boat", p -> new BoatItem(PWEntityTypes.MAPLE_CHEST_BOAT, p), boatProps);
		maple_sign = register("maple_sign", p -> new SignItem(PWBlocks.maple_wall_sign, PWBlocks.maple_sign, p), signProps);
		maple_hanging_sign = register("maple_hanging_sign", p -> new HangingSignItem(PWBlocks.maple_hanging_sign, PWBlocks.maple_wall_hanging_sign, p), signProps);

		tiger_boat = register("tiger_boat", p -> new BoatItem(PWEntityTypes.TIGER_BOAT, p), boatProps);
		tiger_chest_boat = register("tiger_chest_boat", p -> new BoatItem(PWEntityTypes.TIGER_CHEST_BOAT, p), boatProps);
		tiger_sign = register("tiger_sign", p -> new SignItem(PWBlocks.tiger_wall_sign, PWBlocks.tiger_sign, p), signProps);
		tiger_hanging_sign = register("tiger_hanging_sign", p -> new HangingSignItem(PWBlocks.tiger_hanging_sign, PWBlocks.tiger_wall_hanging_sign, p), signProps);

		silverbell_boat = register("silverbell_boat", p -> new BoatItem(PWEntityTypes.SILVERBELL_BOAT, p), boatProps);
		silverbell_chest_boat = register("silverbell_chest_boat", p -> new BoatItem(PWEntityTypes.SILVERBELL_BOAT, p), boatProps);
		silverbell_sign = register("silverbell_sign", p -> new SignItem(PWBlocks.silverbell_wall_sign, PWBlocks.silverbell_sign, p), signProps);
		silverbell_hanging_sign = register("silverbell_hanging_sign", p -> new HangingSignItem(PWBlocks.silverbell_hanging_sign, PWBlocks.silverbell_wall_hanging_sign, p), signProps);

		purple_heart_boat = register("purple_heart_boat", p -> new BoatItem(PWEntityTypes.PURPLE_HEART_BOAT, p), boatProps);
		purple_heart_chest_boat = register("purple_heart_chest_boat", p -> new BoatItem(PWEntityTypes.PURPLE_HEART_CHEST_BOAT, p), boatProps);
		purple_heart_sign = register("purple_heart_sign", p -> new SignItem(PWBlocks.purple_heart_wall_sign, PWBlocks.purple_heart_sign, p), signProps);
		purple_heart_hanging_sign = register("purple_heart_hanging_sign", p -> new HangingSignItem(PWBlocks.purple_heart_hanging_sign, PWBlocks.purple_heart_wall_hanging_sign, p), signProps);

		willow_boat = register("willow_boat", p -> new BoatItem(PWEntityTypes.WILLOW_BOAT, p), boatProps);
		willow_chest_boat = register("willow_chest_boat", p -> new BoatItem(PWEntityTypes.WILLOW_CHEST_BOAT, p), boatProps);
		willow_sign = register("willow_sign", p -> new SignItem(PWBlocks.willow_wall_sign, PWBlocks.willow_sign, p), signProps);
		willow_hanging_sign = register("willow_hanging_sign", p -> new HangingSignItem(PWBlocks.willow_hanging_sign, PWBlocks.willow_wall_hanging_sign, p), signProps);

		magic_boat = register("magic_boat", p -> new BoatItem(PWEntityTypes.MAGIC_BOAT, p), boatProps);
		magic_chest_boat = register("magic_chest_boat", p -> new BoatItem(PWEntityTypes.MAGIC_CHEST_BOAT, p), boatProps);
		magic_sign = register("magic_sign", p -> new SignItem(PWBlocks.magic_wall_sign, PWBlocks.magic_sign, p), signProps);
		magic_hanging_sign = register("magic_hanging_sign", p -> new HangingSignItem(PWBlocks.magic_hanging_sign, PWBlocks.magic_wall_hanging_sign, p), signProps);
	}

	private static void registerBlockItems()
	{
		for (Entry<String, Block> block : PWBlocks.blockItemList.entrySet())
			register(block.getKey(), p -> new BlockItem(block.getValue(), p), () -> new Item.Properties().useBlockDescriptionPrefix());

		PWBlocks.blockItemList.clear();
	}

	@SuppressWarnings("unused")
	private static Item register(String name, Function<Item.Properties, Item> itemFunc)
	{
		return register(name, itemFunc, null);
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc, @Nullable Supplier<Item.Properties> newProps)
	{
		if (newProps == null)
			newProps = () -> new Item.Properties();

		Item.Properties props = newProps.get().setId(key(name));
		Item item = itemFunc.apply(props);

		registryEvent.register(Registries.ITEM, PremiumWoodMod.locate(name), () -> item);
		return item;
	}

	private static ResourceKey<Item> key(String path)
	{
		return ResourceKey.create(Registries.ITEM, PremiumWoodMod.locate(path));
	}
}