package com.legacy.premium_wood.registry;

import java.util.function.Supplier;

import com.legacy.premium_wood.PremiumWoodMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.vehicle.Boat;
import net.minecraft.world.entity.vehicle.ChestBoat;
import net.minecraft.world.item.Item;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PWEntityTypes
{
	public static final EntityType<Boat> MAPLE_BOAT = buildEntity("maple_boat", EntityType.Builder.of(boatFactory(() -> PWItems.maple_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));
	public static final EntityType<ChestBoat> MAPLE_CHEST_BOAT = buildEntity("maple_chest_boat", EntityType.Builder.of(chestBoatFactory(() -> PWItems.maple_chest_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));

	public static final EntityType<Boat> TIGER_BOAT = buildEntity("tiger_boat", EntityType.Builder.of(boatFactory(() -> PWItems.tiger_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));
	public static final EntityType<ChestBoat> TIGER_CHEST_BOAT = buildEntity("tiger_chest_boat", EntityType.Builder.of(chestBoatFactory(() -> PWItems.tiger_chest_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));

	public static final EntityType<Boat> SILVERBELL_BOAT = buildEntity("silverbell_boat", EntityType.Builder.of(boatFactory(() -> PWItems.silverbell_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));
	public static final EntityType<ChestBoat> SILVERBELL_CHEST_BOAT = buildEntity("silverbell_chest_boat", EntityType.Builder.of(chestBoatFactory(() -> PWItems.silverbell_chest_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));

	public static final EntityType<Boat> PURPLE_HEART_BOAT = buildEntity("purple_heart_boat", EntityType.Builder.of(boatFactory(() -> PWItems.purple_heart_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));
	public static final EntityType<ChestBoat> PURPLE_HEART_CHEST_BOAT = buildEntity("purple_heart_chest_boat", EntityType.Builder.of(chestBoatFactory(() -> PWItems.purple_heart_chest_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));

	public static final EntityType<Boat> WILLOW_BOAT = buildEntity("willow_boat", EntityType.Builder.of(boatFactory(() -> PWItems.willow_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));
	public static final EntityType<ChestBoat> WILLOW_CHEST_BOAT = buildEntity("willow_chest_boat", EntityType.Builder.of(chestBoatFactory(() -> PWItems.willow_chest_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));

	public static final EntityType<Boat> MAGIC_BOAT = buildEntity("magic_boat", EntityType.Builder.of(boatFactory(() -> PWItems.magic_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));
	public static final EntityType<ChestBoat> MAGIC_CHEST_BOAT = buildEntity("magic_chest_boat", EntityType.Builder.of(chestBoatFactory(() -> PWItems.magic_chest_boat), MobCategory.MISC).noLootTable().sized(1.375F, 0.5625F).eyeHeight(0.5625F).clientTrackingRange(10));

	public static void init(RegisterEvent e)
	{
		register(e, "maple_boat", PWEntityTypes.MAPLE_BOAT);
		register(e, "maple_chest_boat", PWEntityTypes.MAPLE_CHEST_BOAT);

		register(e, "tiger_boat", PWEntityTypes.TIGER_BOAT);
		register(e, "tiger_chest_boat", PWEntityTypes.TIGER_CHEST_BOAT);

		register(e, "silverbell_boat", PWEntityTypes.SILVERBELL_BOAT);
		register(e, "silverbell_chest_boat", PWEntityTypes.SILVERBELL_CHEST_BOAT);

		register(e, "purple_heart_boat", PWEntityTypes.PURPLE_HEART_BOAT);
		register(e, "purple_heart_chest_boat", PWEntityTypes.PURPLE_HEART_CHEST_BOAT);

		register(e, "willow_boat", PWEntityTypes.WILLOW_BOAT);
		register(e, "willow_chest_boat", PWEntityTypes.WILLOW_CHEST_BOAT);

		register(e, "magic_boat", PWEntityTypes.MAGIC_BOAT);
		register(e, "magic_chest_boat", PWEntityTypes.MAGIC_CHEST_BOAT);
	}

	private static void register(RegisterEvent event, String key, EntityType<?> type)
	{
		event.register(Registries.ENTITY_TYPE, PremiumWoodMod.locate(key), () -> type);
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(ResourceKey.create(Registries.ENTITY_TYPE, PremiumWoodMod.locate(key)));
	}

	private static EntityType.EntityFactory<Boat> boatFactory(Supplier<Item> p_376580_)
	{
		return (p_375558_, p_375559_) -> new Boat(p_375558_, p_375559_, p_376580_);
	}

	private static EntityType.EntityFactory<ChestBoat> chestBoatFactory(Supplier<Item> p_376578_)
	{
		return (p_375555_, p_375556_) -> new ChestBoat(p_375555_, p_375556_, p_376578_);
	}
}
