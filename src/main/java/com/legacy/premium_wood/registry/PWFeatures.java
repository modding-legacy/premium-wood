package com.legacy.premium_wood.registry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block.natural.AppleLeavesBlock;
import com.legacy.premium_wood.data.PWTags;
import com.legacy.premium_wood.world.tree.WeepingFoliagePlacer;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FancyFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.MegaJungleFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.PineFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.FancyTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.data.loading.DatagenModLoader;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class PWFeatures
{
	private static final Map<ResourceLocation, ConfiguredValues<? extends FeatureConfiguration>> CONFIGURED_VALUES = new HashMap<>();
	private static final Map<ResourceLocation, PlacedValues> PLACED_VALUES = new HashMap<>();

	public static final ResourceKey<ConfiguredFeature<?, ?>> APPLE_TREE = conf("apple_tree", Feature.TREE, PWFeatures.Configs.APPLE_TREE_CONFIG);
	public static final ResourceKey<ConfiguredFeature<?, ?>> MAPLE_TREE = conf("maple_tree", Feature.TREE, PWFeatures.Configs.MAPLE_TREE_CONFIG);
	public static final ResourceKey<ConfiguredFeature<?, ?>> SILVERBELL_TREE = conf("silverbell_tree", Feature.TREE, PWFeatures.Configs.SILVERBELL_TREE_CONFIG);
	public static final ResourceKey<ConfiguredFeature<?, ?>> TIGER_TREE = conf("tiger_tree", Feature.TREE, PWFeatures.Configs.TIGER_TREE_CONFIG);
	public static final ResourceKey<ConfiguredFeature<?, ?>> PURPLE_HEART_TREE = conf("purple_heart_tree", Feature.TREE, PWFeatures.Configs.PURPLE_HEART_TREE_CONFIG);
	public static final ResourceKey<ConfiguredFeature<?, ?>> MAGIC_TREE = conf("magic_tree", Feature.TREE, PWFeatures.Configs.MAGIC_TREE_CONFIG);
	public static final ResourceKey<ConfiguredFeature<?, ?>> WILLOW_TREE = conf("willow_tree", Feature.TREE, PWFeatures.Configs.WILLOW_TREE_CONFIG);

	public static final ResourceKey<PlacedFeature> PLACABLE_APPLE_TREE = place("apple_tree", PWFeatures.APPLE_TREE, () -> VegetationPlacements.treePlacement(PlacementUtils.countExtra(0, 0.05F, 1), PWBlocks.apple_sapling));
	public static final ResourceKey<PlacedFeature> PLACABLE_MAPLE_TREE = place("maple_tree", PWFeatures.MAPLE_TREE, () -> VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), PWBlocks.maple_sapling));
	public static final ResourceKey<PlacedFeature> PLACABLE_SILVERBELL_TREE = place("silverbell_tree", PWFeatures.SILVERBELL_TREE, () -> VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), PWBlocks.silverbell_sapling));
	public static final ResourceKey<PlacedFeature> PLACABLE_TIGER_TREE = place("tiger_tree", PWFeatures.TIGER_TREE, () -> VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), PWBlocks.tiger_sapling));
	public static final ResourceKey<PlacedFeature> PLACABLE_PURPLE_HEART_TREE = place("purple_heart_tree", PWFeatures.PURPLE_HEART_TREE, () -> VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), PWBlocks.purple_heart_sapling));
	public static final ResourceKey<PlacedFeature> PLACABLE_MAGIC_TREE = place("magic_tree", PWFeatures.MAGIC_TREE, () -> VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(6), PWBlocks.magic_sapling));
	public static final ResourceKey<PlacedFeature> PLACABLE_WILLOW_TREE = place("willow_tree", PWFeatures.WILLOW_TREE, () -> VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), PWBlocks.willow_sapling));

	public static final ResourceKey<BiomeModifier> APPLE_MODIFIER = modifier("add_apple_trees");
	public static final ResourceKey<BiomeModifier> MAPLE_MODIFIER = modifier("add_maple_trees");
	public static final ResourceKey<BiomeModifier> SILVERBELL_MODIFIER = modifier("add_silverbell_trees");
	public static final ResourceKey<BiomeModifier> TIGER_MODIFIER = modifier("add_tiger_trees");
	public static final ResourceKey<BiomeModifier> PURPLE_HEART_MODIFIER = modifier("add_purple_heart_trees");
	public static final ResourceKey<BiomeModifier> WILLOW_MODIFIER = modifier("add_willow_trees");
	public static final ResourceKey<BiomeModifier> MAGIC_MODIFIER = modifier("add_magic_trees");

	private static <FC extends FeatureConfiguration, F extends Feature<FC>> ResourceKey<ConfiguredFeature<?, ?>> conf(String name, F feature, Lazy<FC> config)
	{
		var key = ResourceKey.create(Registries.CONFIGURED_FEATURE, PremiumWoodMod.locate(name));

		if (DatagenModLoader.isRunningDataGen())
			CONFIGURED_VALUES.put(PremiumWoodMod.locate(name), new ConfiguredValues<FC>(key, feature, config));

		return key;
	}

	private static ResourceKey<PlacedFeature> place(String name, ResourceKey<ConfiguredFeature<?, ?>> configuredFeatureKey, Supplier<List<PlacementModifier>> modifiers)
	{
		var key = ResourceKey.create(Registries.PLACED_FEATURE, PremiumWoodMod.locate(name));

		if (DatagenModLoader.isRunningDataGen())
			PLACED_VALUES.put(PremiumWoodMod.locate(name), new PlacedValues(key, configuredFeatureKey, Lazy.of(modifiers)));

		return key;
	}

	private static ResourceKey<BiomeModifier> modifier(String name)
	{
		return ResourceKey.create(NeoForgeRegistries.Keys.BIOME_MODIFIERS, PremiumWoodMod.locate(name));
	}

	@SuppressWarnings("unchecked")
	public static void configuredBootstrap(BootstrapContext<ConfiguredFeature<?, ?>> bootstrap)
	{
		CONFIGURED_VALUES.forEach((key, value) -> FeatureUtils.register(bootstrap, value.key(), value.feature(), value.config().get()));
		CONFIGURED_VALUES.clear();
	}

	public static void placedBootstrap(BootstrapContext<PlacedFeature> bootstrap)
	{
		PLACED_VALUES.forEach((key, value) -> PlacementUtils.register(bootstrap, value.key(), bootstrap.lookup(Registries.CONFIGURED_FEATURE).getOrThrow(value.configuredFeatureKey()), value.modifiers().get()));
		PLACED_VALUES.clear();
	}

	public static void modifierBootstrap(BootstrapContext<BiomeModifier> bootstrap)
	{
		var biome = bootstrap.lookup(Registries.BIOME);
		var feature = bootstrap.lookup(Registries.PLACED_FEATURE);

		bootstrap.register(APPLE_MODIFIER, createBiomeModifier(PWTags.Biomes.APPLE_SPAWN_BIOMES, PWFeatures.PLACABLE_APPLE_TREE, biome, feature));
		bootstrap.register(MAPLE_MODIFIER, createBiomeModifier(PWTags.Biomes.MAPLE_SPAWN_BIOMES, PWFeatures.PLACABLE_MAPLE_TREE, biome, feature));
		bootstrap.register(SILVERBELL_MODIFIER, createBiomeModifier(PWTags.Biomes.SILVERBELL_SPAWN_BIOMES, PWFeatures.PLACABLE_SILVERBELL_TREE, biome, feature));
		bootstrap.register(TIGER_MODIFIER, createBiomeModifier(PWTags.Biomes.TIGER_SPAWN_BIOMES, PWFeatures.PLACABLE_TIGER_TREE, biome, feature));
		bootstrap.register(PURPLE_HEART_MODIFIER, createBiomeModifier(PWTags.Biomes.PURPLE_HEART_SPAWN_BIOMES, PWFeatures.PLACABLE_PURPLE_HEART_TREE, biome, feature));
		bootstrap.register(WILLOW_MODIFIER, createBiomeModifier(PWTags.Biomes.WILLOW_SPAWN_BIOMES, PWFeatures.PLACABLE_WILLOW_TREE, biome, feature));
		bootstrap.register(MAGIC_MODIFIER, createBiomeModifier(PWTags.Biomes.MAGIC_SPAWN_BIOMES, PWFeatures.PLACABLE_MAGIC_TREE, biome, feature));
	}

	private static BiomeModifier createBiomeModifier(TagKey<Biome> spawnTag, ResourceKey<PlacedFeature> feature, HolderGetter<Biome> biomeLookup, HolderGetter<PlacedFeature> featureLookup)
	{
		return new BiomeModifiers.AddFeaturesBiomeModifier(biomeLookup.getOrThrow(spawnTag), HolderSet.direct(featureLookup.getOrThrow(feature)), GenerationStep.Decoration.VEGETAL_DECORATION);
	}

	public static class Configs
	{
		public static final Lazy<TreeConfiguration> APPLE_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(Blocks.OAK_LOG), new StraightTrunkPlacer(4, 2, 0), new WeightedStateProvider(SimpleWeightedRandomList.<BlockState>builder().add(PWBlocks.apple_leaves.defaultBlockState(), 6).add(PWBlocks.apple_leaves.defaultBlockState().setValue(AppleLeavesBlock.CAN_MATURE, true).setValue(AppleLeavesBlock.MATURE, true), 1)), new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());

		public static final Lazy<TreeConfiguration> MAPLE_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(PWBlocks.maple_log), new StraightTrunkPlacer(4, 2, 0), BlockStateProvider.simple(PWBlocks.maple_leaves), new FancyFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
		public static final Lazy<TreeConfiguration> SILVERBELL_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(PWBlocks.silverbell_log), new StraightTrunkPlacer(4, 2, 0), BlockStateProvider.simple(PWBlocks.silverbell_leaves), new BlobFoliagePlacer(ConstantInt.of(3), ConstantInt.of(2), 3), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
		public static final Lazy<TreeConfiguration> TIGER_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(PWBlocks.tiger_log), new StraightTrunkPlacer(6, 2, 0), BlockStateProvider.simple(PWBlocks.tiger_leaves), new MegaJungleFoliagePlacer(ConstantInt.of(1), ConstantInt.of(0), 0), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
		public static final Lazy<TreeConfiguration> PURPLE_HEART_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(PWBlocks.purple_heart_log), new FancyTrunkPlacer(10, 8, 2), BlockStateProvider.simple(PWBlocks.purple_heart_leaves), new MegaJungleFoliagePlacer(ConstantInt.of(1), ConstantInt.of(1), 0), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
		public static final Lazy<TreeConfiguration> MAGIC_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(PWBlocks.magic_log), new StraightTrunkPlacer(6, 2, 0), BlockStateProvider.simple(PWBlocks.magic_leaves), new PineFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), ConstantInt.of(3)), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
		public static final Lazy<TreeConfiguration> WILLOW_TREE_CONFIG = Lazy.of(() -> new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(PWBlocks.willow_log), new StraightTrunkPlacer(7, 0, 0), BlockStateProvider.simple(PWBlocks.willow_leaves), new WeepingFoliagePlacer(ConstantInt.of(3), ConstantInt.of(0), 4), new TwoLayersFeatureSize(1, 0, 1)).ignoreVines().build());
	}

	@SuppressWarnings("rawtypes")
	private record ConfiguredValues<FC extends FeatureConfiguration>(ResourceKey<ConfiguredFeature<?, ?>> key, Feature feature, Lazy<FC> config)
	{
	}

	private record PlacedValues(ResourceKey<PlacedFeature> key, ResourceKey<ConfiguredFeature<?, ?>> configuredFeatureKey, Lazy<List<PlacementModifier>> modifiers)
	{
	}
}