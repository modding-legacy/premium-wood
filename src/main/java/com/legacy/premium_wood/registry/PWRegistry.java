package com.legacy.premium_wood.registry;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.world.tree.WeepingFoliagePlacer;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FoliagePlacerType;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = PremiumWoodMod.MODID, bus = Bus.MOD)
public class PWRegistry
{
	public static final Lazy<FoliagePlacerType<?>> WILLOW = Lazy.of(() -> new FoliagePlacerType<WeepingFoliagePlacer>(WeepingFoliagePlacer.WILLOW_CODEC));

	@SubscribeEvent
	public static void onRegisterItems(RegisterEvent event)
	{
		ResourceKey<? extends Registry<?>> registry = event.getRegistryKey();

		if (registry.equals(Registries.ENTITY_TYPE))
			PWEntityTypes.init(event);
		else if (registry.equals(Registries.ITEM))
			PWItems.init(event);
		else if (registry.equals(Registries.BLOCK))
			PWBlocks.init(event);
		else if (registry.equals(Registries.FOLIAGE_PLACER_TYPE))
			event.register(Registries.FOLIAGE_PLACER_TYPE, PremiumWoodMod.locate("willow_foliage_placer"), WILLOW);
		else if (registry.equals(Registries.BLOCK_ENTITY_TYPE))
			PWBlockEntityTypes.init(event);
		else if (registry.equals(Registries.BLOCK_TYPE))
			PWBlockTypes.init(event);
	}
}