package com.legacy.premium_wood.registry;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;

public class PWCreativeTabs
{
	public static void init(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.TOOLS_AND_UTILITIES)
		{
			List<ItemLike> orderedItems = new ArrayList<>();

			orderedItems.add(PWItems.maple_boat);
			orderedItems.add(PWItems.maple_chest_boat);

			orderedItems.add(PWItems.tiger_boat);
			orderedItems.add(PWItems.tiger_chest_boat);

			orderedItems.add(PWItems.silverbell_boat);
			orderedItems.add(PWItems.silverbell_chest_boat);

			orderedItems.add(PWItems.purple_heart_boat);
			orderedItems.add(PWItems.purple_heart_chest_boat);

			orderedItems.add(PWItems.willow_boat);
			orderedItems.add(PWItems.willow_chest_boat);

			orderedItems.add(PWItems.magic_boat);
			orderedItems.add(PWItems.magic_chest_boat);

			insertAfter(event, orderedItems, Items.BAMBOO_CHEST_RAFT);
		}

		if (event.getTabKey() == CreativeModeTabs.BUILDING_BLOCKS)
		{
			insertAfter(event, List.of(PWBlocks.oak_framed_glass), Items.OAK_BUTTON);
			insertAfter(event, List.of(PWBlocks.birch_framed_glass), Items.BIRCH_BUTTON);
			insertAfter(event, List.of(PWBlocks.spruce_framed_glass), Items.SPRUCE_BUTTON);
			insertAfter(event, List.of(PWBlocks.jungle_framed_glass), Items.JUNGLE_BUTTON);
			insertAfter(event, List.of(PWBlocks.acacia_framed_glass), Items.ACACIA_BUTTON);
			insertAfter(event, List.of(PWBlocks.dark_oak_framed_glass), Items.DARK_OAK_BUTTON);
			insertAfter(event, List.of(PWBlocks.mangrove_framed_glass), Items.MANGROVE_BUTTON);
			insertAfter(event, List.of(PWBlocks.bamboo_framed_glass), Items.BAMBOO_BUTTON);
			insertAfter(event, List.of(PWBlocks.cherry_framed_glass), Items.CHERRY_BUTTON);
			insertAfter(event, List.of(PWBlocks.pale_oak_framed_glass), Items.PALE_OAK_BUTTON);

			insertAfter(event, List.of(PWBlocks.crimson_framed_glass), Items.CRIMSON_BUTTON);

			List<ItemLike> orderedItems = new ArrayList<>();

			orderedItems.add(PWBlocks.warped_framed_glass);

			orderedItems.add(PWBlocks.maple_log);
			orderedItems.add(PWBlocks.maple_wood);
			orderedItems.add(PWBlocks.stripped_maple_log);
			orderedItems.add(PWBlocks.stripped_maple_wood);
			orderedItems.add(PWBlocks.maple_planks);
			orderedItems.add(PWBlocks.maple_stairs);
			orderedItems.add(PWBlocks.maple_slab);
			orderedItems.add(PWBlocks.maple_fence);
			orderedItems.add(PWBlocks.maple_fence_gate);
			orderedItems.add(PWBlocks.maple_door);
			orderedItems.add(PWBlocks.maple_trapdoor);
			orderedItems.add(PWBlocks.maple_pressure_plate);
			orderedItems.add(PWBlocks.maple_button);
			orderedItems.add(PWBlocks.maple_framed_glass);

			orderedItems.add(PWBlocks.tiger_log);
			orderedItems.add(PWBlocks.tiger_wood);
			orderedItems.add(PWBlocks.stripped_tiger_log);
			orderedItems.add(PWBlocks.stripped_tiger_wood);
			orderedItems.add(PWBlocks.tiger_planks);
			orderedItems.add(PWBlocks.tiger_stairs);
			orderedItems.add(PWBlocks.tiger_slab);
			orderedItems.add(PWBlocks.tiger_fence);
			orderedItems.add(PWBlocks.tiger_fence_gate);
			orderedItems.add(PWBlocks.tiger_door);
			orderedItems.add(PWBlocks.tiger_trapdoor);
			orderedItems.add(PWBlocks.tiger_pressure_plate);
			orderedItems.add(PWBlocks.tiger_button);
			orderedItems.add(PWBlocks.tiger_framed_glass);

			orderedItems.add(PWBlocks.silverbell_log);
			orderedItems.add(PWBlocks.silverbell_wood);
			orderedItems.add(PWBlocks.stripped_silverbell_log);
			orderedItems.add(PWBlocks.stripped_silverbell_wood);
			orderedItems.add(PWBlocks.silverbell_planks);
			orderedItems.add(PWBlocks.silverbell_stairs);
			orderedItems.add(PWBlocks.silverbell_slab);
			orderedItems.add(PWBlocks.silverbell_fence);
			orderedItems.add(PWBlocks.silverbell_fence_gate);
			orderedItems.add(PWBlocks.silverbell_door);
			orderedItems.add(PWBlocks.silverbell_trapdoor);
			orderedItems.add(PWBlocks.silverbell_pressure_plate);
			orderedItems.add(PWBlocks.silverbell_button);
			orderedItems.add(PWBlocks.silverbell_framed_glass);

			orderedItems.add(PWBlocks.purple_heart_log);
			orderedItems.add(PWBlocks.purple_heart_wood);
			orderedItems.add(PWBlocks.stripped_purple_heart_log);
			orderedItems.add(PWBlocks.stripped_purple_heart_wood);
			orderedItems.add(PWBlocks.purple_heart_planks);
			orderedItems.add(PWBlocks.purple_heart_stairs);
			orderedItems.add(PWBlocks.purple_heart_slab);
			orderedItems.add(PWBlocks.purple_heart_fence);
			orderedItems.add(PWBlocks.purple_heart_fence_gate);
			orderedItems.add(PWBlocks.purple_heart_door);
			orderedItems.add(PWBlocks.purple_heart_trapdoor);
			orderedItems.add(PWBlocks.purple_heart_pressure_plate);
			orderedItems.add(PWBlocks.purple_heart_button);
			orderedItems.add(PWBlocks.purple_heart_framed_glass);

			orderedItems.add(PWBlocks.willow_log);
			orderedItems.add(PWBlocks.willow_wood);
			orderedItems.add(PWBlocks.stripped_willow_log);
			orderedItems.add(PWBlocks.stripped_willow_wood);
			orderedItems.add(PWBlocks.willow_planks);
			orderedItems.add(PWBlocks.willow_stairs);
			orderedItems.add(PWBlocks.willow_slab);
			orderedItems.add(PWBlocks.willow_fence);
			orderedItems.add(PWBlocks.willow_fence_gate);
			orderedItems.add(PWBlocks.willow_door);
			orderedItems.add(PWBlocks.willow_trapdoor);
			orderedItems.add(PWBlocks.willow_pressure_plate);
			orderedItems.add(PWBlocks.willow_button);
			orderedItems.add(PWBlocks.willow_framed_glass);

			orderedItems.add(PWBlocks.magic_log);
			orderedItems.add(PWBlocks.magic_wood);
			orderedItems.add(PWBlocks.stripped_magic_log);
			orderedItems.add(PWBlocks.stripped_magic_wood);
			orderedItems.add(PWBlocks.magic_planks);
			orderedItems.add(PWBlocks.magic_stairs);
			orderedItems.add(PWBlocks.magic_slab);
			orderedItems.add(PWBlocks.magic_fence);
			orderedItems.add(PWBlocks.magic_fence_gate);
			orderedItems.add(PWBlocks.magic_door);
			orderedItems.add(PWBlocks.magic_trapdoor);
			orderedItems.add(PWBlocks.magic_pressure_plate);
			orderedItems.add(PWBlocks.magic_button);
			orderedItems.add(PWBlocks.magic_framed_glass);

			insertAfter(event, orderedItems, Items.WARPED_BUTTON);
		}

		if (event.getTabKey() == CreativeModeTabs.NATURAL_BLOCKS)
		{
			List<ItemLike> logs = new ArrayList<>();
			logs.add(PWBlocks.maple_log);
			logs.add(PWBlocks.tiger_log);
			logs.add(PWBlocks.silverbell_log);
			logs.add(PWBlocks.purple_heart_log);
			logs.add(PWBlocks.willow_log);
			logs.add(PWBlocks.magic_log);
			insertAfter(event, logs, Items.WARPED_STEM);

			List<ItemLike> leaves = new ArrayList<>();
			leaves.add(PWBlocks.apple_leaves);
			leaves.add(PWBlocks.maple_leaves);
			leaves.add(PWBlocks.tiger_leaves);
			leaves.add(PWBlocks.silverbell_leaves);
			leaves.add(PWBlocks.purple_heart_leaves);
			leaves.add(PWBlocks.willow_leaves);
			leaves.add(PWBlocks.magic_leaves);
			insertAfter(event, leaves, Items.FLOWERING_AZALEA_LEAVES);

			List<ItemLike> saplings = new ArrayList<>();
			saplings.add(PWBlocks.apple_sapling);
			saplings.add(PWBlocks.maple_sapling);
			saplings.add(PWBlocks.tiger_sapling);
			saplings.add(PWBlocks.silverbell_sapling);
			saplings.add(PWBlocks.purple_heart_sapling);
			saplings.add(PWBlocks.willow_sapling);
			saplings.add(PWBlocks.magic_sapling);
			insertAfter(event, saplings, Items.FLOWERING_AZALEA);
		}

		if (event.getTabKey() == CreativeModeTabs.FUNCTIONAL_BLOCKS)
		{
			List<ItemLike> orderedItems = new ArrayList<>();
			orderedItems.add(PWItems.maple_sign);
			orderedItems.add(PWItems.maple_hanging_sign);
			orderedItems.add(PWItems.tiger_sign);
			orderedItems.add(PWItems.tiger_hanging_sign);
			orderedItems.add(PWItems.silverbell_sign);
			orderedItems.add(PWItems.silverbell_hanging_sign);
			orderedItems.add(PWItems.purple_heart_sign);
			orderedItems.add(PWItems.purple_heart_hanging_sign);
			orderedItems.add(PWItems.willow_sign);
			orderedItems.add(PWItems.willow_hanging_sign);
			orderedItems.add(PWItems.magic_sign);
			orderedItems.add(PWItems.magic_hanging_sign);

			insertAfter(event, orderedItems, Items.WARPED_HANGING_SIGN);
		}
	}

	private static void insertAfter(BuildCreativeModeTabContentsEvent event, List<ItemLike> items, ItemLike target)
	{
		ItemStack currentStack = null;

		for (ItemStack e : event.getParentEntries())
		{
			if (e.getItem() == target)
			{
				currentStack = e;
				break;
			}
		}

		for (var item : items)
			event.insertAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
	}
}
