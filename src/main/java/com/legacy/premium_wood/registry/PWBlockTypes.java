package com.legacy.premium_wood.registry;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block.FramedGlassBlock;
import com.legacy.premium_wood.block.PremiumBookshelfBlock;
import com.legacy.premium_wood.block.PremiumFlowerPotBlock;
import com.legacy.premium_wood.block.PremiumWorkbenchBlock;
import com.legacy.premium_wood.block.natural.AppleLeavesBlock;
import com.legacy.premium_wood.block.natural.ExplodingSaplingBlock;
import com.legacy.premium_wood.block.natural.MagicLeavesBlock;
import com.legacy.premium_wood.block.natural.WillowLeavesBlock;
import com.legacy.premium_wood.block.sign.PremiumCeilingHangingSignBlock;
import com.legacy.premium_wood.block.sign.PremiumStandingSignBlock;
import com.legacy.premium_wood.block.sign.PremiumWallHangingSignBlock;
import com.legacy.premium_wood.block.sign.PremiumWallSignBlock;

import net.minecraft.core.registries.Registries;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PWBlockTypes
{
	public static void init(RegisterEvent event)
	{
		event.register(Registries.BLOCK_TYPE, helper ->
		{
			helper.register(PremiumWoodMod.locate("apple_leaves"), AppleLeavesBlock.CODEC);
			helper.register(PremiumWoodMod.locate("magic_leaves"), MagicLeavesBlock.CODEC);
			helper.register(PremiumWoodMod.locate("willow_leaves"), WillowLeavesBlock.CODEC);
			helper.register(PremiumWoodMod.locate("exploding_sapling"), ExplodingSaplingBlock.CODEC);
			
			helper.register(PremiumWoodMod.locate("ceiling_hanging_sign"), PremiumCeilingHangingSignBlock.CODEC);
			helper.register(PremiumWoodMod.locate("wall_hanging_sign"), PremiumWallHangingSignBlock.CODEC);
			helper.register(PremiumWoodMod.locate("standing_sign"), PremiumStandingSignBlock.CODEC);
			helper.register(PremiumWoodMod.locate("wall_sign"), PremiumWallSignBlock.CODEC);
			
			helper.register(PremiumWoodMod.locate("framed_glass"), FramedGlassBlock.CODEC);
			helper.register(PremiumWoodMod.locate("bookshelf"), PremiumBookshelfBlock.CODEC);
			helper.register(PremiumWoodMod.locate("flower_pot"), PremiumFlowerPotBlock.CODEC);
			helper.register(PremiumWoodMod.locate("workbench"), PremiumWorkbenchBlock.CODEC);
		});
	}
}
