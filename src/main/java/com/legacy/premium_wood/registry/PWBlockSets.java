package com.legacy.premium_wood.registry;

import com.legacy.premium_wood.PremiumWoodMod;

import net.minecraft.world.level.block.state.properties.BlockSetType;

public class PWBlockSets
{
	public static final BlockSetType MAPLE = register("maple");
	public static final BlockSetType TIGER = register("tiger");
	public static final BlockSetType SILVERBELL = register("silverbell");
	public static final BlockSetType PURPLE_HEART = register("purple_heart");
	public static final BlockSetType WILLOW = register("willow");
	public static final BlockSetType MAGIC = register("magic");

	public static BlockSetType register(String key)
	{
		// PremiumWoodMod.MODID + "/" +
		return BlockSetType.register(new BlockSetType(PremiumWoodMod.find(key)));
	}

	public static void init()
	{
	}
}
