package com.legacy.premium_wood.registry;

import com.legacy.premium_wood.PremiumWoodMod;

import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.block.state.properties.WoodType;

public class PWWoodTypes
{
	public static final WoodType MAPLE = register("maple", PWBlockSets.MAPLE);
	public static final WoodType TIGER = register("tiger", PWBlockSets.TIGER);
	public static final WoodType SILVERBELL = register("silverbell", PWBlockSets.SILVERBELL);
	public static final WoodType PURPLE_HEART = register("purple_heart", PWBlockSets.PURPLE_HEART);
	public static final WoodType WILLOW = register("willow", PWBlockSets.WILLOW);
	public static final WoodType MAGIC = register("magic", PWBlockSets.MAGIC);

	public static WoodType register(String key, BlockSetType blockSet)
	{
		return WoodType.register(new WoodType(PremiumWoodMod.find(key), blockSet));
	}

	public static void init()
	{
	}
}
