package com.legacy.premium_wood.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block.FramedGlassBlock;
import com.legacy.premium_wood.block.PremiumBookshelfBlock;
import com.legacy.premium_wood.block.PremiumFlowerPotBlock;
import com.legacy.premium_wood.block.PremiumWorkbenchBlock;
import com.legacy.premium_wood.block.natural.AppleLeavesBlock;
import com.legacy.premium_wood.block.natural.ExplodingSaplingBlock;
import com.legacy.premium_wood.block.natural.MagicLeavesBlock;
import com.legacy.premium_wood.block.natural.WillowLeavesBlock;
import com.legacy.premium_wood.block.sign.PremiumCeilingHangingSignBlock;
import com.legacy.premium_wood.block.sign.PremiumStandingSignBlock;
import com.legacy.premium_wood.block.sign.PremiumWallHangingSignBlock;
import com.legacy.premium_wood.block.sign.PremiumWallSignBlock;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.grower.TreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PWBlocks
{
	// @formatter:off
	public static Block maple_crafting_table, maple_leaves, maple_log, maple_wood, stripped_maple_log, stripped_maple_wood, maple_planks, maple_stairs, maple_slab, maple_fence, maple_fence_gate, maple_door, maple_trapdoor, maple_pressure_plate, maple_button, maple_sapling, maple_bookshelf, maple_framed_glass, maple_sign, maple_wall_sign, maple_hanging_sign, maple_wall_hanging_sign;

	public static Block tiger_crafting_table, tiger_leaves, tiger_log, tiger_wood, stripped_tiger_log, stripped_tiger_wood, tiger_planks, tiger_stairs, tiger_slab, tiger_fence, tiger_fence_gate, tiger_door, tiger_trapdoor, tiger_pressure_plate, tiger_button, tiger_sapling, tiger_bookshelf, tiger_framed_glass, tiger_sign, tiger_wall_sign, tiger_hanging_sign, tiger_wall_hanging_sign;

	public static Block silverbell_crafting_table, silverbell_leaves, silverbell_log, silverbell_wood, stripped_silverbell_log, stripped_silverbell_wood, silverbell_planks, silverbell_stairs, silverbell_slab, silverbell_fence, silverbell_fence_gate, silverbell_door, silverbell_trapdoor, silverbell_pressure_plate, silverbell_button, silverbell_sapling, silverbell_bookshelf, silverbell_framed_glass, silverbell_sign, silverbell_wall_sign, silverbell_hanging_sign, silverbell_wall_hanging_sign;

	public static Block purple_heart_crafting_table, purple_heart_leaves, purple_heart_log, purple_heart_wood, stripped_purple_heart_log, stripped_purple_heart_wood, purple_heart_planks, purple_heart_stairs, purple_heart_slab, purple_heart_fence, purple_heart_fence_gate, purple_heart_door, purple_heart_trapdoor, purple_heart_pressure_plate, purple_heart_button, purple_heart_sapling, purple_heart_bookshelf, purple_heart_framed_glass, purple_heart_sign, purple_heart_wall_sign, purple_heart_hanging_sign, purple_heart_wall_hanging_sign;

	public static Block willow_crafting_table, willow_leaves, willow_log, willow_wood, stripped_willow_log, stripped_willow_wood, willow_planks, willow_stairs, willow_slab, willow_fence, willow_fence_gate, willow_door, willow_trapdoor, willow_pressure_plate, willow_button, willow_sapling, willow_bookshelf, willow_framed_glass, willow_sign, willow_wall_sign, willow_hanging_sign, willow_wall_hanging_sign;

	public static Block magic_crafting_table, magic_leaves, magic_log, magic_wood, stripped_magic_log, stripped_magic_wood, magic_planks, magic_stairs, magic_slab, magic_fence, magic_fence_gate, magic_door, magic_trapdoor, magic_pressure_plate, magic_button, magic_sapling, magic_bookshelf, magic_framed_glass, magic_sign, magic_wall_sign, magic_hanging_sign, magic_wall_hanging_sign;

	public static Block apple_leaves, apple_sapling;

	public static Block oak_framed_glass, birch_framed_glass, spruce_framed_glass, jungle_framed_glass, acacia_framed_glass, dark_oak_framed_glass, mangrove_framed_glass, bamboo_framed_glass, cherry_framed_glass,pale_oak_framed_glass, warped_framed_glass, crimson_framed_glass;

	public static Block potted_maple_sapling, potted_tiger_sapling, potted_silverbell_sapling, potted_purple_heart_sapling, potted_willow_sapling, potted_magic_sapling, potted_apple_sapling;
	// @formatter:on

	public static Map<String, Block> blockItemList = new HashMap<>();

	private static RegisterEvent registryEvent;

	public static final Lazy<TreeGrower> MAPLE_GROWER = Lazy.of(() -> new TreeGrower("maple", Optional.empty(), Optional.of(PWFeatures.MAPLE_TREE), Optional.empty()));
	public static final Lazy<TreeGrower> TIGER_GROWER = Lazy.of(() -> new TreeGrower("tiger", Optional.empty(), Optional.of(PWFeatures.TIGER_TREE), Optional.empty()));
	public static final Lazy<TreeGrower> SILVERBELL_GROWER = Lazy.of(() -> new TreeGrower("silverbell", Optional.empty(), Optional.of(PWFeatures.SILVERBELL_TREE), Optional.empty()));
	public static final Lazy<TreeGrower> PURPLE_HEART_GROWER = Lazy.of(() -> new TreeGrower("purple_heart", Optional.empty(), Optional.of(PWFeatures.PURPLE_HEART_TREE), Optional.empty()));
	public static final Lazy<TreeGrower> WILLOW_GROWER = Lazy.of(() -> new TreeGrower("willow", Optional.empty(), Optional.of(PWFeatures.WILLOW_TREE), Optional.empty()));
	public static final Lazy<TreeGrower> MAGIC_GROWER = Lazy.of(() -> new TreeGrower("magic", Optional.empty(), Optional.of(PWFeatures.MAGIC_TREE), Optional.empty()));
	public static final Lazy<TreeGrower> APPLE_GROWER = Lazy.of(() -> new TreeGrower("apple", Optional.empty(), Optional.of(PWFeatures.APPLE_TREE), Optional.empty()));

	public static void init(RegisterEvent event)
	{
		PWBlocks.registryEvent = event;

		Supplier<BlockBehaviour.Properties> leafProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_LEAVES);
		Supplier<BlockBehaviour.Properties> logProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_LOG);
		Supplier<BlockBehaviour.Properties> saplingProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_SAPLING);
		Supplier<BlockBehaviour.Properties> plankProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_PLANKS);
		Supplier<BlockBehaviour.Properties> slabProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_SLAB);
		Supplier<BlockBehaviour.Properties> stairProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_STAIRS);
		Supplier<BlockBehaviour.Properties> fenceProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_FENCE);
		Supplier<BlockBehaviour.Properties> gateProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_FENCE_GATE);
		Supplier<BlockBehaviour.Properties> pressurePlateProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_PRESSURE_PLATE);
		Supplier<BlockBehaviour.Properties> doorProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_DOOR);
		Supplier<BlockBehaviour.Properties> trapdoorProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_TRAPDOOR);
		Supplier<BlockBehaviour.Properties> glassProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.GLASS);
		Supplier<BlockBehaviour.Properties> signProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_SIGN);
		Supplier<BlockBehaviour.Properties> hangingSignProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_HANGING_SIGN);

		Supplier<BlockBehaviour.Properties> bookshelfProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.BOOKSHELF);
		Supplier<BlockBehaviour.Properties> craftingTableProps = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.CRAFTING_TABLE);

		maple_crafting_table = register("maple_crafting_table", PremiumWorkbenchBlock::new, craftingTableProps);
		maple_leaves = register("maple_leaves", LeavesBlock::new, leafProps);
		maple_log = register("maple_log", RotatedPillarBlock::new, logProps);
		maple_wood = register("maple_wood", RotatedPillarBlock::new, logProps);
		stripped_maple_log = register("stripped_maple_log", RotatedPillarBlock::new, logProps);
		stripped_maple_wood = register("stripped_maple_wood", RotatedPillarBlock::new, logProps);
		maple_planks = register("maple_planks", Block::new, plankProps);
		maple_bookshelf = register("maple_bookshelf", PremiumBookshelfBlock::new, bookshelfProps);
		maple_stairs = register("maple_stairs", p -> new StairBlock(PWBlocks.maple_planks.defaultBlockState(), p), stairProps);
		maple_slab = register("maple_slab", SlabBlock::new, slabProps);
		maple_fence = register("maple_fence", FenceBlock::new, fenceProps);
		maple_fence_gate = register("maple_fence_gate", p -> new FenceGateBlock(PWWoodTypes.MAPLE, p), gateProps);
		maple_door = register("maple_door", p -> new DoorBlock(PWBlockSets.MAPLE, p), doorProps);
		maple_trapdoor = register("maple_trapdoor", p -> new TrapDoorBlock(PWBlockSets.MAPLE, p), trapdoorProps);
		maple_pressure_plate = register("maple_pressure_plate", p -> new PressurePlateBlock(PWBlockSets.MAPLE, p), pressurePlateProps);
		maple_button = registerButton("maple_button", PWBlockSets.MAPLE);
		maple_sapling = register("maple_sapling", p -> new SaplingBlock(MAPLE_GROWER.get(), p), saplingProps);
		maple_framed_glass = register("maple_framed_glass", FramedGlassBlock::new, glassProps);
		maple_sign = registerBlock("maple_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.MAPLE, p), signProps);
		maple_wall_sign = registerBlock("maple_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.MAPLE, p), signProps);
		maple_hanging_sign = registerBlock("maple_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.MAPLE, p), hangingSignProps);
		maple_wall_hanging_sign = registerBlock("maple_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.MAPLE, p), hangingSignProps);

		tiger_crafting_table = register("tiger_crafting_table", PremiumWorkbenchBlock::new, craftingTableProps);
		tiger_leaves = register("tiger_leaves", LeavesBlock::new, leafProps);
		tiger_log = register("tiger_log", RotatedPillarBlock::new, logProps);
		tiger_wood = register("tiger_wood", RotatedPillarBlock::new, logProps);
		stripped_tiger_log = register("stripped_tiger_log", RotatedPillarBlock::new, logProps);
		stripped_tiger_wood = register("stripped_tiger_wood", RotatedPillarBlock::new, logProps);
		tiger_planks = register("tiger_planks", Block::new, plankProps);
		tiger_bookshelf = register("tiger_bookshelf", PremiumBookshelfBlock::new, bookshelfProps);
		tiger_stairs = register("tiger_stairs", p -> new StairBlock(PWBlocks.tiger_planks.defaultBlockState(), p), stairProps);
		tiger_slab = register("tiger_slab", SlabBlock::new, slabProps);
		tiger_fence = register("tiger_fence", FenceBlock::new, fenceProps);
		tiger_fence_gate = register("tiger_fence_gate", p -> new FenceGateBlock(PWWoodTypes.TIGER, p), gateProps);
		tiger_door = register("tiger_door", p -> new DoorBlock(PWBlockSets.TIGER, p), doorProps);
		tiger_trapdoor = register("tiger_trapdoor", p -> new TrapDoorBlock(PWBlockSets.TIGER, p), trapdoorProps);
		tiger_pressure_plate = register("tiger_pressure_plate", p -> new PressurePlateBlock(PWBlockSets.TIGER, p), pressurePlateProps);
		tiger_button = registerButton("tiger_button", PWBlockSets.TIGER);
		tiger_sapling = register("tiger_sapling", p -> new SaplingBlock(TIGER_GROWER.get(), p), saplingProps);
		tiger_framed_glass = register("tiger_framed_glass", FramedGlassBlock::new, glassProps);
		tiger_sign = registerBlock("tiger_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.TIGER, p), signProps);
		tiger_wall_sign = registerBlock("tiger_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.TIGER, p), signProps);
		tiger_hanging_sign = registerBlock("tiger_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.TIGER, p), hangingSignProps);
		tiger_wall_hanging_sign = registerBlock("tiger_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.TIGER, p), hangingSignProps);

		silverbell_crafting_table = register("silverbell_crafting_table", PremiumWorkbenchBlock::new, craftingTableProps);
		silverbell_leaves = register("silverbell_leaves", LeavesBlock::new, leafProps);
		silverbell_log = register("silverbell_log", RotatedPillarBlock::new, logProps);
		silverbell_wood = register("silverbell_wood", RotatedPillarBlock::new, logProps);
		stripped_silverbell_log = register("stripped_silverbell_log", RotatedPillarBlock::new, logProps);
		stripped_silverbell_wood = register("stripped_silverbell_wood", RotatedPillarBlock::new, logProps);
		silverbell_planks = register("silverbell_planks", Block::new, plankProps);
		silverbell_bookshelf = register("silverbell_bookshelf", PremiumBookshelfBlock::new, bookshelfProps);
		silverbell_stairs = register("silverbell_stairs", p -> new StairBlock(PWBlocks.silverbell_planks.defaultBlockState(), p), stairProps);
		silverbell_slab = register("silverbell_slab", SlabBlock::new, slabProps);
		silverbell_fence = register("silverbell_fence", FenceBlock::new, fenceProps);
		silverbell_fence_gate = register("silverbell_fence_gate", p -> new FenceGateBlock(PWWoodTypes.SILVERBELL, p), gateProps);
		silverbell_door = register("silverbell_door", p -> new DoorBlock(PWBlockSets.SILVERBELL, p), doorProps);
		silverbell_trapdoor = register("silverbell_trapdoor", p -> new TrapDoorBlock(PWBlockSets.SILVERBELL, p), trapdoorProps);
		silverbell_pressure_plate = register("silverbell_pressure_plate", p -> new PressurePlateBlock(PWBlockSets.SILVERBELL, p), pressurePlateProps);
		silverbell_button = registerButton("silverbell_button", PWBlockSets.SILVERBELL);
		silverbell_sapling = register("silverbell_sapling", p -> new SaplingBlock(SILVERBELL_GROWER.get(), p), saplingProps);
		silverbell_framed_glass = register("silverbell_framed_glass", FramedGlassBlock::new, glassProps);
		silverbell_sign = registerBlock("silverbell_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.SILVERBELL, p), signProps);
		silverbell_wall_sign = registerBlock("silverbell_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.SILVERBELL, p), signProps);
		silverbell_hanging_sign = registerBlock("silverbell_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.SILVERBELL, p), hangingSignProps);
		silverbell_wall_hanging_sign = registerBlock("silverbell_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.SILVERBELL, p), hangingSignProps);

		purple_heart_crafting_table = register("purple_heart_crafting_table", PremiumWorkbenchBlock::new, craftingTableProps);
		purple_heart_leaves = register("purple_heart_leaves", LeavesBlock::new, leafProps);
		purple_heart_log = register("purple_heart_log", RotatedPillarBlock::new, logProps);
		purple_heart_wood = register("purple_heart_wood", RotatedPillarBlock::new, logProps);
		stripped_purple_heart_log = register("stripped_purple_heart_log", RotatedPillarBlock::new, logProps);
		stripped_purple_heart_wood = register("stripped_purple_heart_wood", RotatedPillarBlock::new, logProps);
		purple_heart_planks = register("purple_heart_planks", Block::new, plankProps);
		purple_heart_bookshelf = register("purple_heart_bookshelf", PremiumBookshelfBlock::new, bookshelfProps);
		purple_heart_stairs = register("purple_heart_stairs", p -> new StairBlock(PWBlocks.purple_heart_planks.defaultBlockState(), p), stairProps);
		purple_heart_slab = register("purple_heart_slab", SlabBlock::new, slabProps);
		purple_heart_fence = register("purple_heart_fence", FenceBlock::new, fenceProps);
		purple_heart_fence_gate = register("purple_heart_fence_gate", p -> new FenceGateBlock(PWWoodTypes.PURPLE_HEART, p), gateProps);
		purple_heart_door = register("purple_heart_door", p -> new DoorBlock(PWBlockSets.PURPLE_HEART, p), doorProps);
		purple_heart_trapdoor = register("purple_heart_trapdoor", p -> new TrapDoorBlock(PWBlockSets.PURPLE_HEART, p), trapdoorProps);
		purple_heart_pressure_plate = register("purple_heart_pressure_plate", p -> new PressurePlateBlock(PWBlockSets.PURPLE_HEART, p), pressurePlateProps);
		purple_heart_button = registerButton("purple_heart_button", PWBlockSets.PURPLE_HEART);
		purple_heart_sapling = register("purple_heart_sapling", p -> new SaplingBlock(PURPLE_HEART_GROWER.get(), p), saplingProps);
		purple_heart_framed_glass = register("purple_heart_framed_glass", FramedGlassBlock::new, glassProps);
		purple_heart_sign = registerBlock("purple_heart_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.PURPLE_HEART, p), signProps);
		purple_heart_wall_sign = registerBlock("purple_heart_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.PURPLE_HEART, p), signProps);
		purple_heart_hanging_sign = registerBlock("purple_heart_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.PURPLE_HEART, p), hangingSignProps);
		purple_heart_wall_hanging_sign = registerBlock("purple_heart_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.PURPLE_HEART, p), hangingSignProps);

		willow_crafting_table = register("willow_crafting_table", PremiumWorkbenchBlock::new, craftingTableProps);
		willow_leaves = register("willow_leaves", WillowLeavesBlock::new, leafProps);
		willow_log = register("willow_log", RotatedPillarBlock::new, logProps);
		willow_wood = register("willow_wood", RotatedPillarBlock::new, logProps);
		stripped_willow_log = register("stripped_willow_log", RotatedPillarBlock::new, logProps);
		stripped_willow_wood = register("stripped_willow_wood", RotatedPillarBlock::new, logProps);
		willow_planks = register("willow_planks", Block::new, plankProps);
		willow_bookshelf = register("willow_bookshelf", PremiumBookshelfBlock::new, bookshelfProps);
		willow_stairs = register("willow_stairs", p -> new StairBlock(PWBlocks.willow_planks.defaultBlockState(), p), stairProps);
		willow_slab = register("willow_slab", SlabBlock::new, slabProps);
		willow_fence = register("willow_fence", FenceBlock::new, fenceProps);
		willow_fence_gate = register("willow_fence_gate", p -> new FenceGateBlock(PWWoodTypes.WILLOW, p), gateProps);
		willow_door = register("willow_door", p -> new DoorBlock(PWBlockSets.WILLOW, p), doorProps);
		willow_trapdoor = register("willow_trapdoor", p -> new TrapDoorBlock(PWBlockSets.WILLOW, p), trapdoorProps);
		willow_pressure_plate = register("willow_pressure_plate", p -> new PressurePlateBlock(PWBlockSets.MAGIC, p), pressurePlateProps);
		willow_button = registerButton("willow_button", PWBlockSets.WILLOW);
		willow_sapling = register("willow_sapling", p -> new SaplingBlock(WILLOW_GROWER.get(), p), saplingProps);
		willow_framed_glass = register("willow_framed_glass", FramedGlassBlock::new, glassProps);
		willow_sign = registerBlock("willow_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.WILLOW, p), signProps);
		willow_wall_sign = registerBlock("willow_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.WILLOW, p), signProps);
		willow_hanging_sign = registerBlock("willow_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.WILLOW, p), hangingSignProps);
		willow_wall_hanging_sign = registerBlock("willow_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.WILLOW, p), hangingSignProps);

		magic_crafting_table = register("magic_crafting_table", PremiumWorkbenchBlock::new, craftingTableProps);
		magic_leaves = register("magic_leaves", MagicLeavesBlock::new, leafProps);
		magic_log = register("magic_log", RotatedPillarBlock::new, logProps);
		magic_wood = register("magic_wood", RotatedPillarBlock::new, logProps);
		stripped_magic_log = register("stripped_magic_log", RotatedPillarBlock::new, logProps);
		stripped_magic_wood = register("stripped_magic_wood", RotatedPillarBlock::new, logProps);
		magic_planks = register("magic_planks", Block::new, plankProps);
		magic_bookshelf = register("magic_bookshelf", PremiumBookshelfBlock::new, bookshelfProps);
		magic_stairs = register("magic_stairs", p -> new StairBlock(PWBlocks.magic_planks.defaultBlockState(), p), stairProps);
		magic_slab = register("magic_slab", SlabBlock::new, slabProps);
		magic_fence = register("magic_fence", FenceBlock::new, fenceProps);
		magic_fence_gate = register("magic_fence_gate", p -> new FenceGateBlock(PWWoodTypes.MAGIC, p), gateProps);
		magic_door = register("magic_door", p -> new DoorBlock(PWBlockSets.MAGIC, p), doorProps);
		magic_trapdoor = register("magic_trapdoor", p -> new TrapDoorBlock(PWBlockSets.MAGIC, p), trapdoorProps);
		magic_pressure_plate = register("magic_pressure_plate", p -> new PressurePlateBlock(PWBlockSets.MAGIC, p), pressurePlateProps);
		magic_button = registerButton("magic_button", PWBlockSets.MAGIC);
		magic_sapling = register("magic_sapling", p -> new ExplodingSaplingBlock(MAGIC_GROWER.get(), p), saplingProps);
		magic_framed_glass = register("magic_framed_glass", FramedGlassBlock::new, glassProps);
		magic_sign = registerBlock("magic_sign", p -> new PremiumStandingSignBlock(PWWoodTypes.MAGIC, p), signProps);
		magic_wall_sign = registerBlock("magic_wall_sign", p -> new PremiumWallSignBlock(PWWoodTypes.MAGIC, p), signProps);
		magic_hanging_sign = registerBlock("magic_hanging_sign", p -> new PremiumCeilingHangingSignBlock(PWWoodTypes.MAGIC, p), hangingSignProps);
		magic_wall_hanging_sign = registerBlock("magic_wall_hanging_sign", p -> new PremiumWallHangingSignBlock(PWWoodTypes.MAGIC, p), hangingSignProps);

		apple_leaves = register("apple_leaves", p -> new AppleLeavesBlock(p), leafProps);
		apple_sapling = register("apple_sapling", p -> new SaplingBlock(APPLE_GROWER.get(), p), saplingProps);

		oak_framed_glass = register("oak_framed_glass", FramedGlassBlock::new, glassProps);
		birch_framed_glass = register("birch_framed_glass", FramedGlassBlock::new, glassProps);
		spruce_framed_glass = register("spruce_framed_glass", FramedGlassBlock::new, glassProps);
		jungle_framed_glass = register("jungle_framed_glass", FramedGlassBlock::new, glassProps);
		acacia_framed_glass = register("acacia_framed_glass", FramedGlassBlock::new, glassProps);
		dark_oak_framed_glass = register("dark_oak_framed_glass", FramedGlassBlock::new, glassProps);
		mangrove_framed_glass = register("mangrove_framed_glass", FramedGlassBlock::new, glassProps);
		bamboo_framed_glass = register("bamboo_framed_glass", FramedGlassBlock::new, glassProps);
		cherry_framed_glass = register("cherry_framed_glass", FramedGlassBlock::new, glassProps);
		pale_oak_framed_glass = register("pale_oak_framed_glass", FramedGlassBlock::new, glassProps);
		warped_framed_glass = register("warped_framed_glass", FramedGlassBlock::new, glassProps);
		crimson_framed_glass = register("crimson_framed_glass", FramedGlassBlock::new, glassProps);

		potted_maple_sapling = registerPot("potted_maple_sapling", () -> maple_sapling);
		potted_tiger_sapling = registerPot("potted_tiger_sapling", () -> tiger_sapling);
		potted_silverbell_sapling = registerPot("potted_silverbell_sapling", () -> silverbell_sapling);
		potted_purple_heart_sapling = registerPot("potted_purple_heart_sapling", () -> purple_heart_sapling);
		potted_willow_sapling = registerPot("potted_willow_sapling", () -> willow_sapling);
		potted_magic_sapling = registerPot("potted_magic_sapling", () -> magic_sapling);
		potted_apple_sapling = registerPot("potted_apple_sapling", () -> apple_sapling);
	}

	public static Block registerButton(String key, BlockSetType type)
	{
		return register(key, p -> new ButtonBlock(type, 30, p), () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_BUTTON));
	}

	public static Block registerPot(String key, Supplier<Block> flower)
	{
		return register(key, p -> new PremiumFlowerPotBlock(flower, p), () -> BlockBehaviour.Properties.of().strength(0.0F));
	}

	public static Block register(String key, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		Block block = registerBlock(key, blockFunc, newProps);
		blockItemList.put(key, block);
		return block;
	}

	public static Block registerBlock(String name, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		if (registryEvent != null)
		{
			BlockBehaviour.Properties props = newProps.get().setId(key(name));
			Block block = blockFunc.apply(props);

			// System.out.println("registering " + name);
			registryEvent.register(Registries.BLOCK, PremiumWoodMod.locate(name), () -> block);

			return block;
		}

		return Blocks.STONE;
	}

	private static ResourceKey<Block> key(String path)
	{
		return ResourceKey.create(Registries.BLOCK, PremiumWoodMod.locate(path));
	}
}