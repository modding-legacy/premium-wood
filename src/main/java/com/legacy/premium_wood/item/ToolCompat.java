package com.legacy.premium_wood.item;

import java.util.HashMap;
import java.util.Map;

import com.legacy.premium_wood.registry.PWBlocks;

import net.minecraft.world.level.block.Block;

public class ToolCompat
{
	public static final Map<Block, Block> AXE_STRIPPING = new HashMap<>();

	static
	{
		axeStripping(PWBlocks.maple_log, PWBlocks.stripped_maple_log);
		axeStripping(PWBlocks.tiger_log, PWBlocks.stripped_tiger_log);
		axeStripping(PWBlocks.silverbell_log, PWBlocks.stripped_silverbell_log);
		axeStripping(PWBlocks.purple_heart_log, PWBlocks.stripped_purple_heart_log);
		axeStripping(PWBlocks.willow_log, PWBlocks.stripped_willow_log);
		axeStripping(PWBlocks.magic_log, PWBlocks.stripped_magic_log);

		axeStripping(PWBlocks.maple_wood, PWBlocks.stripped_maple_wood);
		axeStripping(PWBlocks.tiger_wood, PWBlocks.stripped_tiger_wood);
		axeStripping(PWBlocks.silverbell_wood, PWBlocks.stripped_silverbell_wood);
		axeStripping(PWBlocks.purple_heart_wood, PWBlocks.stripped_purple_heart_wood);
		axeStripping(PWBlocks.willow_wood, PWBlocks.stripped_willow_wood);
		axeStripping(PWBlocks.magic_wood, PWBlocks.stripped_magic_wood);
	}

	public static void init()
	{
	}

	static void axeStripping(Block log, Block stripped)
	{
		AXE_STRIPPING.put(log, stripped);
	}
}