package com.legacy.premium_wood.data;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.data.PWTagProv.BlockProv;
import com.legacy.premium_wood.registry.PWFeatures;

import net.minecraft.DetectedVersion;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@EventBusSubscriber(modid = PremiumWoodMod.MODID, bus = Bus.MOD)
public class PWDataGen
{
	private static final RegistrySetBuilder BUILDER = new RegistrySetBuilder().add(Registries.CONFIGURED_FEATURE, (c) -> PWFeatures.configuredBootstrap(c)).add(Registries.PLACED_FEATURE, PWFeatures::placedBootstrap).add(NeoForgeRegistries.Keys.BIOME_MODIFIERS, PWFeatures::modifierBootstrap);

	@SubscribeEvent
	public static void gatherData(GatherDataEvent.Client event) throws InterruptedException, ExecutionException
	{
		DataGenerator gen = event.getGenerator();
		CompletableFuture<Provider> lookup = event.getLookupProvider();
		PackOutput output = gen.getPackOutput();
		boolean run = true;

		gen.addProvider(run, new PWLootProv(gen, lookup));

		BlockProv prov = new PWTagProv.BlockProv(gen, lookup);

		gen.addProvider(run, prov);
		gen.addProvider(run, new PWTagProv.ItemProv(gen, prov.contentsGetter(), lookup));
		gen.addProvider(run, new PWTagProv.BiomeProv(gen, lookup));
		gen.addProvider(run, new PWRecipeProv.Runner(output, lookup));
		gen.addProvider(run, new PWDataMapProv(output, lookup));

		gen.addProvider(run, new DatapackBuiltinEntriesProvider(output, event.getLookupProvider(), BUILDER, Set.of(PremiumWoodMod.MODID)));

		gen.addProvider(run, new PWModelProv(output));

		gen.addProvider(run, packMcmeta(output, "Premium Wood resources"));

		PackOutput legacyPackOutput = gen.getPackOutput("assets/premium_wood/legacy_pack");
		gen.addProvider(run, packMcmeta(legacyPackOutput, "The classic look of Premium Wood"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}

	private record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
	{

		public static <D extends DataProvider> NestedDataProvider<D> of(D provider, String namePrefix)
		{
			return new NestedDataProvider<>(provider, namePrefix);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cachedOutput)
		{
			return this.provider.run(cachedOutput);
		}

		@Override
		public String getName()
		{
			return this.namePrefix + "/" + this.provider.getName();
		}
	}
}