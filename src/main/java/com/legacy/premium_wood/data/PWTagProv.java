package com.legacy.premium_wood.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block.PremiumBookshelfBlock;
import com.legacy.premium_wood.block.PremiumWorkbenchBlock;
import com.legacy.premium_wood.registry.PWBlocks;
import com.legacy.premium_wood.registry.PWItems;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.CeilingHangingSignBlock;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;

public class PWTagProv
{
	public static class BlockProv extends BlockTagsProvider
	{
		public BlockProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, PremiumWoodMod.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			premiumWood();
			vanilla();
			forge();
		}

		void premiumWood()
		{
			this.tag(PWTags.Blocks.PLANKS).add(PWBlocks.maple_planks, PWBlocks.tiger_planks, PWBlocks.silverbell_planks, PWBlocks.purple_heart_planks, PWBlocks.willow_planks, PWBlocks.magic_planks);

			this.tag(PWTags.Blocks.MAPLE_LOGS).add(PWBlocks.maple_log, PWBlocks.maple_wood, PWBlocks.stripped_maple_log, PWBlocks.stripped_maple_wood);
			this.tag(PWTags.Blocks.TIGER_LOGS).add(PWBlocks.tiger_log, PWBlocks.tiger_wood, PWBlocks.stripped_tiger_log, PWBlocks.stripped_tiger_wood);
			this.tag(PWTags.Blocks.SILVERBELL_LOGS).add(PWBlocks.silverbell_log, PWBlocks.silverbell_wood, PWBlocks.stripped_silverbell_log, PWBlocks.stripped_silverbell_wood);
			this.tag(PWTags.Blocks.PURPLE_HEART_LOGS).add(PWBlocks.purple_heart_log, PWBlocks.purple_heart_wood, PWBlocks.stripped_purple_heart_log, PWBlocks.stripped_purple_heart_wood);
			this.tag(PWTags.Blocks.WILLOW_LOGS).add(PWBlocks.willow_log, PWBlocks.willow_wood, PWBlocks.stripped_willow_log, PWBlocks.stripped_willow_wood);
			this.tag(PWTags.Blocks.MAGIC_LOGS).add(PWBlocks.magic_log, PWBlocks.magic_wood, PWBlocks.stripped_magic_log, PWBlocks.stripped_magic_wood);

			this.tag(PWTags.Blocks.FRAMED_GLASS).add(PWBlocks.maple_framed_glass, PWBlocks.tiger_framed_glass, PWBlocks.silverbell_framed_glass, PWBlocks.purple_heart_framed_glass, PWBlocks.willow_framed_glass, PWBlocks.magic_framed_glass);
		}

		@SuppressWarnings("unchecked")
		void vanilla()
		{
			addMatching(b -> b instanceof SaplingBlock, BlockTags.SAPLINGS);
			addMatching(b -> b instanceof FlowerPotBlock, BlockTags.FLOWER_POTS);
			addMatching(b -> b instanceof LeavesBlock, BlockTags.LEAVES, BlockTags.MINEABLE_WITH_HOE);
			addMatching(b -> b instanceof ButtonBlock, BlockTags.WOODEN_BUTTONS);
			addMatching(b -> b instanceof DoorBlock, BlockTags.WOODEN_DOORS);
			addMatching(b -> b instanceof FenceBlock, BlockTags.WOODEN_FENCES);
			addMatching(b -> b instanceof PressurePlateBlock, BlockTags.WOODEN_PRESSURE_PLATES);
			addMatching(b -> b instanceof SlabBlock, BlockTags.WOODEN_SLABS);
			addMatching(b -> b instanceof StairBlock, BlockTags.WOODEN_STAIRS);
			addMatching(b -> b instanceof TrapDoorBlock, BlockTags.WOODEN_TRAPDOORS);
			addMatching(b -> b instanceof RotatedPillarBlock, BlockTags.LOGS_THAT_BURN);
			addMatching(b -> b instanceof PremiumBookshelfBlock || b instanceof PremiumWorkbenchBlock, BlockTags.MINEABLE_WITH_AXE);
			addMatching(b -> b instanceof WallSignBlock, BlockTags.WALL_SIGNS);
			addMatching(b -> b instanceof StandingSignBlock, BlockTags.STANDING_SIGNS);
			addMatching(b -> b instanceof CeilingHangingSignBlock, BlockTags.CEILING_HANGING_SIGNS);
			addMatching(b -> b instanceof WallHangingSignBlock, BlockTags.WALL_HANGING_SIGNS);

			this.tag(BlockTags.PLANKS).addTag(PWTags.Blocks.PLANKS);

			this.tag(BlockTags.OVERWORLD_NATURAL_LOGS).addTags(PWTags.Blocks.MAPLE_LOGS, PWTags.Blocks.TIGER_LOGS, PWTags.Blocks.SILVERBELL_LOGS, PWTags.Blocks.PURPLE_HEART_LOGS, PWTags.Blocks.WILLOW_LOGS, PWTags.Blocks.MAGIC_LOGS);
		}

		void forge()
		{
			addMatching(b -> b instanceof ChestBlock, Tags.Blocks.CHESTS_WOODEN);
			addMatching(b -> b instanceof FenceBlock, Tags.Blocks.FENCES_WOODEN);
			addMatching(b -> b instanceof FenceGateBlock, Tags.Blocks.FENCE_GATES_WOODEN);
		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(PremiumWoodMod.MODID) && condition.apply(block));
		}

		@SafeVarargs
		private void addMatching(Function<Block, Boolean> condition, TagKey<Block>... tags)
		{
			for (TagKey<Block> tag : tags)
				getMatching(condition).forEach(this.tag(tag)::add);
		}

		@Override
		public String getName()
		{
			return "Premium Wood Block Tags";
		}
	}

	public static class ItemProv extends ItemTagsProvider
	{
		public ItemProv(DataGenerator generatorIn, CompletableFuture<TagLookup<Block>> blocktagProvIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, blocktagProvIn, PremiumWoodMod.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			premiumWood();
			vanilla();
			forge();
		}

		void premiumWood()
		{
			this.copy(PWTags.Blocks.PLANKS, PWTags.Items.PLANKS);

			this.copy(PWTags.Blocks.MAPLE_LOGS, PWTags.Items.MAPLE_LOGS);
			this.copy(PWTags.Blocks.TIGER_LOGS, PWTags.Items.TIGER_LOGS);
			this.copy(PWTags.Blocks.SILVERBELL_LOGS, PWTags.Items.SILVERBELL_LOGS);
			this.copy(PWTags.Blocks.PURPLE_HEART_LOGS, PWTags.Items.PURPLE_HEART_LOGS);
			this.copy(PWTags.Blocks.WILLOW_LOGS, PWTags.Items.WILLOW_LOGS);
			this.copy(PWTags.Blocks.MAGIC_LOGS, PWTags.Items.MAGIC_LOGS);

			this.copy(PWTags.Blocks.FRAMED_GLASS, PWTags.Items.FRAMED_GLASS);
		}

		void vanilla()
		{
			this.tag(ItemTags.BOATS).add(PWItems.maple_boat, PWItems.tiger_boat, PWItems.silverbell_boat, PWItems.purple_heart_boat, PWItems.willow_boat, PWItems.magic_boat);
			this.tag(ItemTags.CHEST_BOATS).add(PWItems.maple_chest_boat, PWItems.tiger_chest_boat, PWItems.silverbell_chest_boat, PWItems.purple_heart_chest_boat, PWItems.willow_chest_boat, PWItems.magic_chest_boat);

			this.copy(BlockTags.LEAVES, ItemTags.LEAVES);
			this.copy(BlockTags.LOGS_THAT_BURN, ItemTags.LOGS_THAT_BURN);
			this.copy(BlockTags.PLANKS, ItemTags.PLANKS);
			this.copy(BlockTags.SAPLINGS, ItemTags.SAPLINGS);
			// this.copy(BlockTags.SLABS, ItemTags.SLABS);
			// this.copy(BlockTags.STAIRS, ItemTags.STAIRS);
			this.copy(BlockTags.WOODEN_BUTTONS, ItemTags.WOODEN_BUTTONS);
			this.copy(BlockTags.WOODEN_DOORS, ItemTags.WOODEN_DOORS);
			this.copy(BlockTags.WOODEN_FENCES, ItemTags.WOODEN_FENCES);
			this.copy(BlockTags.WOODEN_PRESSURE_PLATES, ItemTags.WOODEN_PRESSURE_PLATES);
			this.copy(BlockTags.WOODEN_SLABS, ItemTags.WOODEN_SLABS);
			this.copy(BlockTags.WOODEN_STAIRS, ItemTags.WOODEN_STAIRS);
			this.copy(BlockTags.WOODEN_TRAPDOORS, ItemTags.WOODEN_TRAPDOORS);
			this.copy(BlockTags.STANDING_SIGNS, ItemTags.SIGNS);
			this.copy(BlockTags.CEILING_HANGING_SIGNS, ItemTags.HANGING_SIGNS);
		}

		void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Premium Wood Item Tags";
		}
	}

	public static class BiomeProv extends BiomeTagsProvider
	{
		public BiomeProv(DataGenerator gen, CompletableFuture<Provider> lookup)
		{
			super(gen.getPackOutput(), lookup, PremiumWoodMod.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			premiumWood();
			vanilla();
			forge();
		}

		void premiumWood()
		{
			this.tag(PWTags.Biomes.APPLE_SPAWN_BIOMES).add(Biomes.PLAINS, Biomes.SUNFLOWER_PLAINS);
			this.tag(PWTags.Biomes.MAPLE_SPAWN_BIOMES).add(Biomes.FOREST, Biomes.FLOWER_FOREST);
			this.tag(PWTags.Biomes.TIGER_SPAWN_BIOMES).add(Biomes.SAVANNA, Biomes.SAVANNA_PLATEAU);
			this.tag(PWTags.Biomes.SILVERBELL_SPAWN_BIOMES).add(Biomes.BIRCH_FOREST, Biomes.OLD_GROWTH_BIRCH_FOREST);
			this.tag(PWTags.Biomes.PURPLE_HEART_SPAWN_BIOMES).add(Biomes.JUNGLE, Biomes.SPARSE_JUNGLE);
			this.tag(PWTags.Biomes.WILLOW_SPAWN_BIOMES).add(Biomes.SWAMP);
			this.tag(PWTags.Biomes.MAGIC_SPAWN_BIOMES).add(Biomes.WINDSWEPT_HILLS, Biomes.TAIGA, Biomes.SNOWY_TAIGA);
		}

		void vanilla()
		{
		}

		void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Premium Wood Biome Tags";
		}
	}
}
