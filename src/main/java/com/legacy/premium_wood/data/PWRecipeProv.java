package com.legacy.premium_wood.data;

import java.util.concurrent.CompletableFuture;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.registry.PWBlocks;
import com.legacy.premium_wood.registry.PWItems;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.advancements.critereon.ImpossibleTrigger;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.neoforge.common.Tags;

public class PWRecipeProv extends VanillaRecipeProvider
{
	public static class Runner extends RecipeProvider.Runner
	{
		public Runner(PackOutput output, CompletableFuture<HolderLookup.Provider> registries)
		{
			super(output, registries);
		}

		@Override
		protected RecipeProvider createRecipeProvider(HolderLookup.Provider lookup, RecipeOutput output)
		{
			return new PWRecipeProv(lookup, output);
		}

		@Override
		public String getName()
		{
			return PremiumWoodMod.NAME + " Recipe Gen";
		}
	}

	
	private HolderGetter<Item> items;

	private PWRecipeProv(HolderLookup.Provider lookupProvider, RecipeOutput output)
	{
		super(lookupProvider, output);
		this.items = lookupProvider.lookupOrThrow(Registries.ITEM);
	}

	@Override
	protected void buildRecipes()
	{
		generateForEnabledBlockFamilies(FeatureFlagSet.of(FeatureFlags.VANILLA));

		framedGlass(PWBlocks.oak_framed_glass, Blocks.OAK_PLANKS);
		framedGlass(PWBlocks.birch_framed_glass, Blocks.BIRCH_PLANKS);
		framedGlass(PWBlocks.spruce_framed_glass, Blocks.SPRUCE_PLANKS);
		framedGlass(PWBlocks.jungle_framed_glass, Blocks.JUNGLE_PLANKS);
		framedGlass(PWBlocks.acacia_framed_glass, Blocks.ACACIA_PLANKS);
		framedGlass(PWBlocks.dark_oak_framed_glass, Blocks.DARK_OAK_PLANKS);
		framedGlass(PWBlocks.mangrove_framed_glass, Blocks.MANGROVE_PLANKS);
		framedGlass(PWBlocks.bamboo_framed_glass, Blocks.BAMBOO_PLANKS);
		framedGlass(PWBlocks.cherry_framed_glass, Blocks.CHERRY_PLANKS);
		framedGlass(PWBlocks.warped_framed_glass, Blocks.WARPED_PLANKS);
		framedGlass(PWBlocks.crimson_framed_glass, Blocks.CRIMSON_PLANKS);

		bookshelf(PWBlocks.maple_bookshelf, PWBlocks.maple_planks);
		planksFromLogs(PWBlocks.maple_planks, PWTags.Items.MAPLE_LOGS, 4);
		woodFromLogs(PWBlocks.maple_wood, PWBlocks.maple_log);
		woodFromLogs(PWBlocks.stripped_maple_wood, PWBlocks.stripped_maple_log);
		woodenBoat(PWItems.maple_boat, PWBlocks.maple_planks);
		chestBoat(PWItems.maple_chest_boat, PWItems.maple_boat);
		twoByTwoPacker(RecipeCategory.DECORATIONS, PWBlocks.maple_crafting_table, PWBlocks.maple_planks);
		hangingSign(PWItems.maple_hanging_sign, PWBlocks.stripped_maple_log);
		framedGlass(PWBlocks.maple_framed_glass, PWBlocks.maple_planks);

		bookshelf(PWBlocks.tiger_bookshelf, PWBlocks.tiger_planks);
		planksFromLogs(PWBlocks.tiger_planks, PWTags.Items.TIGER_LOGS, 4);
		woodFromLogs(PWBlocks.tiger_wood, PWBlocks.tiger_log);
		woodFromLogs(PWBlocks.stripped_tiger_wood, PWBlocks.stripped_tiger_log);
		woodenBoat(PWItems.tiger_boat, PWBlocks.tiger_planks);
		chestBoat(PWItems.tiger_chest_boat, PWItems.tiger_boat);
		twoByTwoPacker(RecipeCategory.DECORATIONS, PWBlocks.tiger_crafting_table, PWBlocks.tiger_planks);
		hangingSign(PWItems.tiger_hanging_sign, PWBlocks.stripped_tiger_log);
		framedGlass(PWBlocks.tiger_framed_glass, PWBlocks.tiger_planks);

		bookshelf(PWBlocks.silverbell_bookshelf, PWBlocks.silverbell_planks);
		planksFromLogs(PWBlocks.silverbell_planks, PWTags.Items.SILVERBELL_LOGS, 4);
		woodFromLogs(PWBlocks.silverbell_wood, PWBlocks.silverbell_log);
		woodFromLogs(PWBlocks.stripped_silverbell_wood, PWBlocks.stripped_silverbell_log);
		woodenBoat(PWItems.silverbell_boat, PWBlocks.silverbell_planks);
		chestBoat(PWItems.silverbell_chest_boat, PWItems.silverbell_boat);
		twoByTwoPacker(RecipeCategory.DECORATIONS, PWBlocks.silverbell_crafting_table, PWBlocks.silverbell_planks);
		hangingSign(PWItems.silverbell_hanging_sign, PWBlocks.stripped_silverbell_log);
		framedGlass(PWBlocks.silverbell_framed_glass, PWBlocks.silverbell_planks);

		bookshelf(PWBlocks.purple_heart_bookshelf, PWBlocks.purple_heart_planks);
		planksFromLogs(PWBlocks.purple_heart_planks, PWTags.Items.PURPLE_HEART_LOGS, 4);
		woodFromLogs(PWBlocks.purple_heart_wood, PWBlocks.purple_heart_log);
		woodFromLogs(PWBlocks.stripped_purple_heart_wood, PWBlocks.stripped_purple_heart_log);
		woodenBoat(PWItems.purple_heart_boat, PWBlocks.purple_heart_planks);
		chestBoat(PWItems.purple_heart_chest_boat, PWItems.purple_heart_boat);
		twoByTwoPacker(RecipeCategory.DECORATIONS, PWBlocks.purple_heart_crafting_table, PWBlocks.purple_heart_planks);
		hangingSign(PWItems.purple_heart_hanging_sign, PWBlocks.stripped_purple_heart_log);
		framedGlass(PWBlocks.purple_heart_framed_glass, PWBlocks.purple_heart_planks);

		bookshelf(PWBlocks.willow_bookshelf, PWBlocks.willow_planks);
		planksFromLogs(PWBlocks.willow_planks, PWTags.Items.WILLOW_LOGS, 4);
		woodFromLogs(PWBlocks.willow_wood, PWBlocks.willow_log);
		woodFromLogs(PWBlocks.stripped_willow_wood, PWBlocks.stripped_willow_log);
		woodenBoat(PWItems.willow_boat, PWBlocks.willow_planks);
		chestBoat(PWItems.willow_chest_boat, PWItems.willow_boat);
		twoByTwoPacker(RecipeCategory.DECORATIONS, PWBlocks.willow_crafting_table, PWBlocks.willow_planks);
		hangingSign(PWItems.willow_hanging_sign, PWBlocks.stripped_willow_log);
		framedGlass(PWBlocks.willow_framed_glass, PWBlocks.willow_planks);

		bookshelf(PWBlocks.magic_bookshelf, PWBlocks.magic_planks);
		planksFromLogs(PWBlocks.magic_planks, PWTags.Items.MAGIC_LOGS, 4);
		woodFromLogs(PWBlocks.magic_wood, PWBlocks.magic_log);
		woodFromLogs(PWBlocks.stripped_magic_wood, PWBlocks.stripped_magic_log);
		woodenBoat(PWItems.magic_boat, PWBlocks.magic_planks);
		chestBoat(PWItems.magic_chest_boat, PWItems.magic_boat);
		twoByTwoPacker(RecipeCategory.DECORATIONS, PWBlocks.magic_crafting_table, PWBlocks.magic_planks);
		hangingSign(PWItems.magic_hanging_sign, PWBlocks.stripped_magic_log);
		framedGlass(PWBlocks.magic_framed_glass, PWBlocks.magic_planks);

		// backup chest recipe, since Quark deletes vanillas
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.DECORATIONS, Blocks.CHEST).define('#', PWTags.Items.PLANKS).pattern("###").pattern("# #").pattern("###").showNotification(false).unlockedBy("impossible", CriteriaTriggers.IMPOSSIBLE.createCriterion(new ImpossibleTrigger.TriggerInstance())).save(this.output, ResourceKey.create(Registries.RECIPE, PremiumWoodMod.locate("chest_backup")));
	}

	private void bookshelf(Block bookshelf, Block planks)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, bookshelf).define('#', planks).define('X', Items.BOOK).pattern("###").pattern("XXX").pattern("###").unlockedBy("has_book", has(Items.BOOK)).save(this.output);
	}

	private void framedGlass(Block glass, Block planks)
	{
		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, glass, 2).requires(Tags.Items.GLASS_BLOCKS_COLORLESS).requires(planks).unlockedBy("has_plank", has(planks.asItem())).save(this.output);
	}

	@Override
	protected void generateForEnabledBlockFamilies(FeatureFlagSet flags)
	{
		/*protected void generateForEnabledBlockFamilies(FeatureFlagSet p_251836_) {
		    BlockFamilies.getAllFamilies().filter(BlockFamily::shouldGenerateRecipe).forEach(p_359455_ -> this.generateRecipes(p_359455_, p_251836_));
		}*/
		PWBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateRecipe).forEach((fam) -> generateRecipes(fam, flags));
	}
}
