package com.legacy.premium_wood.data;

import java.util.ArrayList;
import java.util.List;

import com.legacy.premium_wood.registry.PWBlocks;

import net.minecraft.data.BlockFamily;
import net.minecraft.data.BlockFamily.Builder;
import net.minecraft.world.level.block.Block;

public class PWBlockFamilies
{
	private static final List<BlockFamily> FAMILIES = new ArrayList<>();
	public static final BlockFamily MAPLE_FAMILY = family(PWBlocks.maple_planks).button(PWBlocks.maple_button).fence(PWBlocks.maple_fence).fenceGate(PWBlocks.maple_fence_gate).pressurePlate(PWBlocks.maple_pressure_plate).sign(PWBlocks.maple_sign, PWBlocks.maple_wall_sign).slab(PWBlocks.maple_slab).stairs(PWBlocks.maple_stairs).door(PWBlocks.maple_door).trapdoor(PWBlocks.maple_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();
	public static final BlockFamily TIGER_FAMILY = family(PWBlocks.tiger_planks).button(PWBlocks.tiger_button).fence(PWBlocks.tiger_fence).fenceGate(PWBlocks.tiger_fence_gate).pressurePlate(PWBlocks.tiger_pressure_plate).sign(PWBlocks.tiger_sign, PWBlocks.tiger_wall_sign).slab(PWBlocks.tiger_slab).stairs(PWBlocks.tiger_stairs).door(PWBlocks.tiger_door).trapdoor(PWBlocks.tiger_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();
	public static final BlockFamily SILVERBELL_FAMILY = family(PWBlocks.silverbell_planks).button(PWBlocks.silverbell_button).fence(PWBlocks.silverbell_fence).fenceGate(PWBlocks.silverbell_fence_gate).pressurePlate(PWBlocks.silverbell_pressure_plate).sign(PWBlocks.silverbell_sign, PWBlocks.silverbell_wall_sign).slab(PWBlocks.silverbell_slab).stairs(PWBlocks.silverbell_stairs).door(PWBlocks.silverbell_door).trapdoor(PWBlocks.silverbell_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();
	public static final BlockFamily PURPLE_HEART_FAMILY = family(PWBlocks.purple_heart_planks).button(PWBlocks.purple_heart_button).fence(PWBlocks.purple_heart_fence).fenceGate(PWBlocks.purple_heart_fence_gate).pressurePlate(PWBlocks.purple_heart_pressure_plate).sign(PWBlocks.purple_heart_sign, PWBlocks.purple_heart_wall_sign).slab(PWBlocks.purple_heart_slab).stairs(PWBlocks.purple_heart_stairs).door(PWBlocks.purple_heart_door).trapdoor(PWBlocks.purple_heart_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();
	public static final BlockFamily WILLOW_FAMILY = family(PWBlocks.willow_planks).button(PWBlocks.willow_button).fence(PWBlocks.willow_fence).fenceGate(PWBlocks.willow_fence_gate).pressurePlate(PWBlocks.willow_pressure_plate).sign(PWBlocks.willow_sign, PWBlocks.willow_wall_sign).slab(PWBlocks.willow_slab).stairs(PWBlocks.willow_stairs).door(PWBlocks.willow_door).trapdoor(PWBlocks.willow_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();
	public static final BlockFamily MAGIC_FAMILY = family(PWBlocks.magic_planks).button(PWBlocks.magic_button).fence(PWBlocks.magic_fence).fenceGate(PWBlocks.magic_fence_gate).pressurePlate(PWBlocks.magic_pressure_plate).sign(PWBlocks.magic_sign, PWBlocks.magic_wall_sign).slab(PWBlocks.magic_slab).stairs(PWBlocks.magic_stairs).door(PWBlocks.magic_door).trapdoor(PWBlocks.magic_trapdoor).recipeGroupPrefix("wooden").recipeUnlockedBy("has_planks").getFamily();

	private static Builder family(Block block)
	{
		var builder = new BlockFamily.Builder(block);

		FAMILIES.add(builder.getFamily());

		return builder;
	}

	public static List<BlockFamily> getFamilies()
	{
		return FAMILIES;
	}
}