package com.legacy.premium_wood.data;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block.PremiumBookshelfBlock;
import com.legacy.premium_wood.registry.PWBlocks;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.WritableRegistry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.util.ProblemReporter;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.BonusLevelTableCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class PWLootProv extends LootTableProvider
{
	public PWLootProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(PremiumBlockLoot::new, LootContextParamSets.BLOCK)), lookup);
	}

	@Override
	protected void validate(WritableRegistry<LootTable> writableregistry, ValidationContext validationcontext, ProblemReporter.Collector problemreporter$collector)
	{
		writableregistry.listElements().forEach(lootTable -> lootTable.value().validate(validationcontext.setContextKeySet(lootTable.value().getParamSet()).enterElement("{" + lootTable.key().location() + "}", lootTable.key())));
	}

	private static class PremiumBlockLoot extends BlockLootSubProvider
	{
		protected PremiumBlockLoot(HolderLookup.Provider lookup)
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags(), lookup);
		}

		private final float[] DEFAULT_SAPLING_DROP_RATES = new float[] { 0.05F, 0.0625F, 0.083333336F, 0.1F };

		@Override
		protected void generate()
		{

			blocks().forEach(block ->
			{
				if (block == PWBlocks.apple_leaves)
					add(block, (b) -> leaves(b, PWBlocks.apple_sapling, Items.STICK));
				else if (block == PWBlocks.maple_leaves)
					add(block, (b) -> leaves(b, PWBlocks.maple_sapling, Items.STICK));
				else if (block == PWBlocks.tiger_leaves)
					add(block, (b) -> leaves(b, PWBlocks.tiger_sapling, Items.STICK));
				else if (block == PWBlocks.magic_leaves)
					add(block, (b) -> leaves(b, PWBlocks.magic_sapling, Items.STICK));
				else if (block == PWBlocks.purple_heart_leaves)
					add(block, (b) -> leaves(b, PWBlocks.purple_heart_sapling, Items.STICK));
				else if (block == PWBlocks.silverbell_leaves)
					add(block, (b) -> leaves(b, PWBlocks.silverbell_sapling, Items.STICK));
				else if (block == PWBlocks.willow_leaves)
					add(block, (b) -> leaves(b, PWBlocks.willow_sapling, Items.STICK));
				else if (block instanceof PremiumBookshelfBlock)
					add(block, (b) -> createSingleItemTableWithSilkTouch(block, Items.BOOK, ConstantValue.exactly(3)));
				else if (block instanceof SlabBlock)
					add(block, createSlabItemTable(block));
				else if (block instanceof DoorBlock)
					add(block, (b) -> createSinglePropConditionTable(b, DoorBlock.HALF, DoubleBlockHalf.LOWER));
				else if (block instanceof FlowerPotBlock)
					dropPottedContents(block);
				else
					dropSelf(block);
			});
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(PremiumWoodMod.MODID));
		}

		private LootTable.Builder leaves(Block block, ItemLike sapling, ItemLike stick)
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			return createSilkTouchOrShearsDispatchTable(block, applyExplosionCondition(block, LootItem.lootTableItem(sapling)).when(BonusLevelTableCondition.bonusLevelFlatChance(registrylookup.getOrThrow(Enchantments.FORTUNE), DEFAULT_SAPLING_DROP_RATES))).withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).when(this.hasShears().invert()).add(applyExplosionDecay(block, LootItem.lootTableItem(stick).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 2.0F)))).when(BonusLevelTableCondition.bonusLevelFlatChance(registrylookup.getOrThrow(Enchantments.FORTUNE), 0.02F, 0.022222223F, 0.025F, 0.033333335F, 0.1F))));
		}

	}
}
