package com.legacy.premium_wood.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import com.legacy.premium_wood.PremiumWoodMod;
import com.legacy.premium_wood.block.natural.AppleLeavesBlock;
import com.legacy.premium_wood.registry.PWBlocks;
import com.legacy.premium_wood.registry.PWItems;

import net.minecraft.client.data.models.BlockModelGenerators;
import net.minecraft.client.data.models.ItemModelGenerators;
import net.minecraft.client.data.models.ItemModelOutput;
import net.minecraft.client.data.models.ModelProvider;
import net.minecraft.client.data.models.blockstates.BlockStateGenerator;
import net.minecraft.client.data.models.blockstates.MultiVariantGenerator;
import net.minecraft.client.data.models.blockstates.PropertyDispatch;
import net.minecraft.client.data.models.blockstates.Variant;
import net.minecraft.client.data.models.blockstates.VariantProperties;
import net.minecraft.client.data.models.model.ModelInstance;
import net.minecraft.client.data.models.model.ModelTemplate;
import net.minecraft.client.data.models.model.ModelTemplates;
import net.minecraft.client.data.models.model.TextureMapping;
import net.minecraft.client.data.models.model.TexturedModel;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;

public class PWModelProv extends ModelProvider
{
	public PWModelProv(PackOutput output)
	{
		super(output, PremiumWoodMod.MODID);
	}

	@Override
	public CompletableFuture<?> run(CachedOutput output)
	{
		return super.run(output);
	}

	@Override
	protected void registerModels(BlockModelGenerators blockModels, ItemModelGenerators itemModels)
	{
		var items = new Items(itemModels.itemModelOutput, itemModels.modelOutput);
		items.run();

		var states = new States(blockModels.blockStateOutput, blockModels.itemModelOutput, blockModels.modelOutput);
		states.run();
	}

	/*protected static List<Block> getAllBlocks()
	{
		return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(PremiumWoodMod.MODID)).toList();
	}
	
	protected static List<Item> getAllItems(Function<Item, Boolean> condition)
	{
		return BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(PremiumWoodMod.MODID) && condition.apply(item)).toList();
	}
	
	protected static List<Item> getAllItems()
	{
		return getAllItems(i -> true);
	}*/

	public static class Items extends ItemModelGenerators
	{
		public Items(ItemModelOutput output, BiConsumer<ResourceLocation, ModelInstance> models)
		{
			super(output, models);
		}

		@Override
		public void run()
		{
			this.basicItem(PWItems.maple_boat);
			this.basicItem(PWItems.maple_chest_boat);

			this.basicItem(PWItems.tiger_boat);
			this.basicItem(PWItems.tiger_chest_boat);

			this.basicItem(PWItems.silverbell_boat);
			this.basicItem(PWItems.silverbell_chest_boat);

			this.basicItem(PWItems.purple_heart_boat);
			this.basicItem(PWItems.purple_heart_chest_boat);

			this.basicItem(PWItems.willow_boat);
			this.basicItem(PWItems.willow_chest_boat);

			this.basicItem(PWItems.magic_boat);
			this.basicItem(PWItems.magic_chest_boat);
		}

		private void basicItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_ITEM);
		}

		/*public ItemModelBuilder handheldItem(Item item)
		{
			return handheldItem(Objects.requireNonNull(BuiltInRegistries.ITEM.getKey(item)));
		}
		
		public ItemModelBuilder handheldItem(ResourceLocation item)
		{
			return getBuilder(item.toString()).parent(new ModelFile.UncheckedModelFile("item/handheld")).texture("layer0", ResourceLocation.fromNamespaceAndPath(item.getNamespace(), "item/" + item.getPath()));
		}
		
		public ItemModelBuilder basicItemInLoc(String name, ResourceLocation item)
		{
			return getBuilder(name.replace("block/", "item/")).parent(new ModelFile.UncheckedModelFile("item/generated")).texture("layer0", item);
		}
		
		public ItemModelBuilder basicItemInLoc(ResourceLocation item)
		{
			return this.basicItemInLoc(item.toString(), item);
		}*/

		/*private ResourceLocation key(Item item)
		{
			return BuiltInRegistries.ITEM.getKey(item);
		}
		
		private String name(Item item)
		{
			return key(item).getPath();
		}
		
		private ResourceLocation extend(ResourceLocation rl, String suffix)
		{
			return new ResourceLocation(rl.getNamespace(), rl.getPath() + suffix);
		}*/

		/*public ResourceLocation texLoc(String key)
		{
			return this.modLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}
		
		public ResourceLocation mcTexLoc(String key)
		{
			return this.mcLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}*/

	}

	/*public static class BlockModels extends BlockModelProvider
	{
		public BlockModels(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, PremiumWoodMod.MODID, existingFileHelper);
		}
	
		@Override
		protected void registerModels()
		{
		}
	}*/

	public static class States extends BlockModelGenerators
	{
		public States(Consumer<BlockStateGenerator> blockStateOutput, ItemModelOutput itemModelOutput, BiConsumer<ResourceLocation, ModelInstance> modelOutput)
		{
			super(blockStateOutput, itemModelOutput, modelOutput);
		}

		@Override
		public void run()
		{
			PWBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateModel).forEach(fam -> this.family(fam.getBaseBlock()).generateFor(fam));

			this.simpleBlock(PWBlocks.oak_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.birch_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.spruce_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.jungle_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.acacia_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.dark_oak_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.mangrove_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.bamboo_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.cherry_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.warped_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.crimson_framed_glass, "cutout");
			this.simpleBlock(PWBlocks.pale_oak_framed_glass, "cutout");

			this.createAppleLeaves();
			this.createPlantWithDefaultItem(PWBlocks.apple_sapling, PWBlocks.potted_apple_sapling, BlockModelGenerators.PlantType.NOT_TINTED);

			this.woodProvider(PWBlocks.maple_log).logWithHorizontal(PWBlocks.maple_log).wood(PWBlocks.maple_wood);
			this.woodProvider(PWBlocks.stripped_maple_log).logWithHorizontal(PWBlocks.stripped_maple_log).wood(PWBlocks.stripped_maple_wood);
			this.createHangingSign(PWBlocks.stripped_maple_log, PWBlocks.maple_hanging_sign, PWBlocks.maple_wall_hanging_sign);
			this.createPlantWithDefaultItem(PWBlocks.maple_sapling, PWBlocks.potted_maple_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(PWBlocks.maple_leaves, TexturedModel.LEAVES);
			this.bookshelf(PWBlocks.maple_bookshelf, PWBlocks.maple_planks);
			/*this.blockEntity(PremiumBlocks.maple_chest, PremiumBlocks.maple_planks);*/
			this.craftingTable(PWBlocks.maple_crafting_table, PWBlocks.maple_planks);
			/*this.ladder(PremiumBlocks.maple_ladder);*/
			/*this.vine(PremiumBlocks.maple_vine);*/
			this.simpleBlock(PWBlocks.maple_framed_glass, "cutout");

			this.woodProvider(PWBlocks.tiger_log).logWithHorizontal(PWBlocks.tiger_log).wood(PWBlocks.tiger_wood);
			this.woodProvider(PWBlocks.stripped_tiger_log).logWithHorizontal(PWBlocks.stripped_tiger_log).wood(PWBlocks.stripped_tiger_wood);
			this.createHangingSign(PWBlocks.stripped_tiger_log, PWBlocks.tiger_hanging_sign, PWBlocks.tiger_wall_hanging_sign);
			this.createPlantWithDefaultItem(PWBlocks.tiger_sapling, PWBlocks.potted_tiger_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(PWBlocks.tiger_leaves, TexturedModel.LEAVES);
			this.bookshelf(PWBlocks.tiger_bookshelf, PWBlocks.tiger_planks);
			/*this.blockEntity(PremiumBlocks.tiger_chest, PremiumBlocks.tiger_planks);*/
			this.craftingTable(PWBlocks.tiger_crafting_table, PWBlocks.tiger_planks);
			/*this.ladder(PremiumBlocks.tiger_ladder);*/
			/*this.vine(PremiumBlocks.tiger_vine);*/
			this.simpleBlock(PWBlocks.tiger_framed_glass, "cutout");

			this.woodProvider(PWBlocks.silverbell_log).logWithHorizontal(PWBlocks.silverbell_log).wood(PWBlocks.silverbell_wood);
			this.woodProvider(PWBlocks.stripped_silverbell_log).logWithHorizontal(PWBlocks.stripped_silverbell_log).wood(PWBlocks.stripped_silverbell_wood);
			this.createHangingSign(PWBlocks.stripped_silverbell_log, PWBlocks.silverbell_hanging_sign, PWBlocks.silverbell_wall_hanging_sign);
			this.createPlantWithDefaultItem(PWBlocks.silverbell_sapling, PWBlocks.potted_silverbell_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(PWBlocks.silverbell_leaves, TexturedModel.LEAVES);
			this.bookshelf(PWBlocks.silverbell_bookshelf, PWBlocks.silverbell_planks);
			/*this.blockEntity(PremiumBlocks.silverbell_chest, PremiumBlocks.silverbell_planks);*/
			this.craftingTable(PWBlocks.silverbell_crafting_table, PWBlocks.silverbell_planks);
			/*this.ladder(PremiumBlocks.silverbell_ladder);*/
			/*this.vine(PremiumBlocks.silverbell_vine);*/
			this.simpleBlock(PWBlocks.silverbell_framed_glass, "cutout");

			this.woodProvider(PWBlocks.purple_heart_log).logWithHorizontal(PWBlocks.purple_heart_log).wood(PWBlocks.purple_heart_wood);
			this.woodProvider(PWBlocks.stripped_purple_heart_log).logWithHorizontal(PWBlocks.stripped_purple_heart_log).wood(PWBlocks.stripped_purple_heart_wood);
			this.createHangingSign(PWBlocks.stripped_purple_heart_log, PWBlocks.purple_heart_hanging_sign, PWBlocks.purple_heart_wall_hanging_sign);
			this.createPlantWithDefaultItem(PWBlocks.purple_heart_sapling, PWBlocks.potted_purple_heart_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(PWBlocks.purple_heart_leaves, TexturedModel.LEAVES);
			this.bookshelf(PWBlocks.purple_heart_bookshelf, PWBlocks.purple_heart_planks);
			/*this.blockEntity(PremiumBlocks.purple_heart_chest, PremiumBlocks.purple_heart_planks);*/
			this.craftingTable(PWBlocks.purple_heart_crafting_table, PWBlocks.purple_heart_planks);
			/*this.ladder(PremiumBlocks.purple_heart_ladder);*/
			/*this.vine(PremiumBlocks.purple_heart_vine);*/
			this.simpleBlock(PWBlocks.purple_heart_framed_glass, "cutout");

			this.woodProvider(PWBlocks.willow_log).logWithHorizontal(PWBlocks.willow_log).wood(PWBlocks.willow_wood);
			this.woodProvider(PWBlocks.stripped_willow_log).logWithHorizontal(PWBlocks.stripped_willow_log).wood(PWBlocks.stripped_willow_wood);
			this.createHangingSign(PWBlocks.stripped_willow_log, PWBlocks.willow_hanging_sign, PWBlocks.willow_wall_hanging_sign);
			this.createPlantWithDefaultItem(PWBlocks.willow_sapling, PWBlocks.potted_willow_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(PWBlocks.willow_leaves, TexturedModel.LEAVES);
			this.bookshelf(PWBlocks.willow_bookshelf, PWBlocks.willow_planks);
			/*this.blockEntity(PremiumBlocks.willow_chest, PremiumBlocks.willow_planks);*/
			this.craftingTable(PWBlocks.willow_crafting_table, PWBlocks.willow_planks);
			/*this.ladder(PremiumBlocks.willow_ladder);*/
			/*this.vine(PremiumBlocks.willow_vine);*/
			this.simpleBlock(PWBlocks.willow_framed_glass, "cutout");

			this.woodProvider(PWBlocks.magic_log).logWithHorizontal(PWBlocks.magic_log).wood(PWBlocks.magic_wood);
			this.woodProvider(PWBlocks.stripped_magic_log).logWithHorizontal(PWBlocks.stripped_magic_log).wood(PWBlocks.stripped_magic_wood);
			this.createHangingSign(PWBlocks.stripped_magic_log, PWBlocks.magic_hanging_sign, PWBlocks.magic_wall_hanging_sign);
			this.createPlantWithDefaultItem(PWBlocks.magic_sapling, PWBlocks.potted_magic_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(PWBlocks.magic_leaves, TexturedModel.LEAVES);
			this.bookshelf(PWBlocks.magic_bookshelf, PWBlocks.magic_planks);
			/*this.blockEntity(PremiumBlocks.magic_chest, PremiumBlocks.magic_planks);*/
			this.craftingTable(PWBlocks.magic_crafting_table, PWBlocks.magic_planks);
			/*this.ladder(PremiumBlocks.magic_ladder);*/
			/*this.vine(PremiumBlocks.magic_vine);*/
			this.simpleBlock(PWBlocks.magic_framed_glass, "cutout");
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createPlant(Block flower, Block pot, BlockModelGenerators.PlantType type)
		{
			this.createCrossBlock(flower, type);
			TextureMapping texturemapping = type.getPlantTextureMapping(flower);
			ResourceLocation resourcelocation = type.getCrossPot().extend().renderType(ResourceLocation.parse("cutout")).build().create(pot, texturemapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(pot, resourcelocation));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createCrossBlock(Block flower, BlockModelGenerators.PlantType type, TextureMapping mapping)
		{
			ResourceLocation resourcelocation = type.getCross().extend().renderType(ResourceLocation.parse("cutout")).build().create(flower, mapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(flower, resourcelocation));
		}

		public void vine(Block b)
		{
			this.createMultifaceBlockStates(b);
			ResourceLocation resourcelocation = this.createFlatItemModelWithBlockTexture(b.asItem(), b);
			this.registerSimpleItemModel(b, resourcelocation);
		}

		public void craftingTable(Block block, Block bottom)
		{
			this.createCraftingTableLike(block, bottom, TextureMapping::craftingTable);
		}

		public void ladder(Block block)
		{
			this.createNonTemplateHorizontalBlock(block);
			this.registerSimpleFlatItemModel(block);
		}

		public void createAppleLeaves()
		{
			Block block = PWBlocks.apple_leaves;

			ResourceLocation baseModel = ModelTemplates.CUBE_ALL.create(block, TextureMapping.cube(block), this.modelOutput);
			ResourceLocation matureModel = this.createSuffixedVariant(block, "_mature", ModelTemplates.CUBE_ALL, TextureMapping::cube);
			ResourceLocation goldenModel = this.createSuffixedVariant(block, "_golden", ModelTemplates.CUBE_ALL, TextureMapping::cube);

			this.registerSimpleItemModel(block, matureModel);

			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(block).with(PropertyDispatch.properties(AppleLeavesBlock.MATURE, AppleLeavesBlock.GOLDEN).generate((mature, golden) -> Variant.variant().with(VariantProperties.MODEL, mature ? (golden ? goldenModel : matureModel) : baseModel))));
		}

		/**
		 * Override exists to give them the cutout rendertype
		 */
		@Override
		public void createDoor(Block door)
		{
			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.door(door);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation5 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation6 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation7 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			this.registerSimpleFlatItemModel(door.asItem());
			this.blockStateOutput.accept(createDoor(door, resourcelocation, resourcelocation1, resourcelocation2, resourcelocation3, resourcelocation4, resourcelocation5, resourcelocation6, resourcelocation7));
		}

		private static ModelTemplate extendWithType(String renderType, ModelTemplate template)
		{
			return template.extend().renderType(ResourceLocation.parse(renderType)).build();
		}

		public void bookshelf(Block block, Block plank)
		{
			TextureMapping texturemapping = TextureMapping.column(TextureMapping.getBlockTexture(block), TextureMapping.getBlockTexture(plank));
			ResourceLocation resourcelocation = ModelTemplates.CUBE_COLUMN.create(block, texturemapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(block, resourcelocation));
		}

		public void simpleBlock(Block block)
		{
			this.simpleBlock(block, (String) null);
		}

		public void simpleBlock(Block block, @Nullable String renderType)
		{
			this.createTrivialBlock(block, renderType != null ? TexturedModel.createDefault(TextureMapping::cube, ModelTemplates.CUBE_ALL.extend().renderType(ResourceLocation.parse(renderType)).build()) : TexturedModel.CUBE);
		}
	}
}
