package com.legacy.premium_wood.data;

import com.legacy.premium_wood.PremiumWoodMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;

public class PWTags
{
	public static interface Blocks
	{
		TagKey<Block> PLANKS = tag("planks");

		TagKey<Block> MAPLE_LOGS = tag("maple_logs");
		TagKey<Block> TIGER_LOGS = tag("tiger_logs");
		TagKey<Block> SILVERBELL_LOGS = tag("silverbell_logs");
		TagKey<Block> PURPLE_HEART_LOGS = tag("purple_heart_logs");
		TagKey<Block> WILLOW_LOGS = tag("willow_logs");
		TagKey<Block> MAGIC_LOGS = tag("magic_logs");
		TagKey<Block> FRAMED_GLASS = tag("framed_glass");

		private static TagKey<Block> tag(String name)
		{
			return BlockTags.create(PremiumWoodMod.locate(name));
		}
	}

	public static interface Items
	{
		TagKey<Item> PLANKS = tag("planks");

		TagKey<Item> MAPLE_LOGS = tag("maple_logs");
		TagKey<Item> TIGER_LOGS = tag("tiger_logs");
		TagKey<Item> SILVERBELL_LOGS = tag("silverbell_logs");
		TagKey<Item> PURPLE_HEART_LOGS = tag("purple_heart_logs");
		TagKey<Item> WILLOW_LOGS = tag("willow_logs");
		TagKey<Item> MAGIC_LOGS = tag("magic_logs");
		TagKey<Item> FRAMED_GLASS = tag("framed_glass");

		private static TagKey<Item> tag(String name)
		{
			return ItemTags.create(PremiumWoodMod.locate(name));
		}
	}

	public static interface Biomes
	{
		TagKey<Biome> APPLE_SPAWN_BIOMES = tag("apple_spawn_biomes");
		TagKey<Biome> MAPLE_SPAWN_BIOMES = tag("maple_spawn_biomes");
		TagKey<Biome> TIGER_SPAWN_BIOMES = tag("tiger_spawn_biomes");
		TagKey<Biome> SILVERBELL_SPAWN_BIOMES = tag("silverbell_spawn_biomes");
		TagKey<Biome> PURPLE_HEART_SPAWN_BIOMES = tag("purple_heart_spawn_biomes");
		TagKey<Biome> WILLOW_SPAWN_BIOMES = tag("willow_spawn_biomes");
		TagKey<Biome> MAGIC_SPAWN_BIOMES = tag("magic_spawn_biomes");

		private static TagKey<Biome> tag(String name)
		{
			return TagKey.create(Registries.BIOME, PremiumWoodMod.locate(name));
		}
	}
}
