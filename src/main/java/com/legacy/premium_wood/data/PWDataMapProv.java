package com.legacy.premium_wood.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.legacy.premium_wood.registry.PWBlocks;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.DataMapProvider;
import net.neoforged.neoforge.registries.datamaps.builtin.Compostable;
import net.neoforged.neoforge.registries.datamaps.builtin.NeoForgeDataMaps;

public class PWDataMapProv extends DataMapProvider
{
	public PWDataMapProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		super(packOutput, lookupProvider);
	}

	private final Map<Block, Block> strips = new HashMap<>();

	@SuppressWarnings("deprecation")
	@Override
	protected void gather(HolderLookup.Provider provider)
	{
		List<Block> compostables = List.of(PWBlocks.apple_sapling, PWBlocks.maple_sapling, PWBlocks.tiger_sapling, PWBlocks.silverbell_sapling, PWBlocks.purple_heart_sapling, PWBlocks.willow_sapling, PWBlocks.magic_sapling, PWBlocks.apple_leaves, PWBlocks.maple_leaves, PWBlocks.tiger_leaves, PWBlocks.silverbell_leaves, PWBlocks.purple_heart_leaves, PWBlocks.willow_leaves, PWBlocks.magic_leaves);
		var compBuilder = this.builder(NeoForgeDataMaps.COMPOSTABLES);

		for (var comp : compostables)
			compBuilder.add(comp.asItem().builtInRegistryHolder().getKey(), new Compostable(0.3F), false);

		axeStripping(PWBlocks.maple_log, PWBlocks.stripped_maple_log);
		axeStripping(PWBlocks.tiger_log, PWBlocks.stripped_tiger_log);
		axeStripping(PWBlocks.silverbell_log, PWBlocks.stripped_silverbell_log);
		axeStripping(PWBlocks.purple_heart_log, PWBlocks.stripped_purple_heart_log);
		axeStripping(PWBlocks.willow_log, PWBlocks.stripped_willow_log);
		axeStripping(PWBlocks.magic_log, PWBlocks.stripped_magic_log);

		axeStripping(PWBlocks.maple_wood, PWBlocks.stripped_maple_wood);
		axeStripping(PWBlocks.tiger_wood, PWBlocks.stripped_tiger_wood);
		axeStripping(PWBlocks.silverbell_wood, PWBlocks.stripped_silverbell_wood);
		axeStripping(PWBlocks.purple_heart_wood, PWBlocks.stripped_purple_heart_wood);
		axeStripping(PWBlocks.willow_wood, PWBlocks.stripped_willow_wood);
		axeStripping(PWBlocks.magic_wood, PWBlocks.stripped_magic_wood);
	}

	void axeStripping(Block log, Block stripped)
	{
		this.strips.put(log, stripped);
	}
}