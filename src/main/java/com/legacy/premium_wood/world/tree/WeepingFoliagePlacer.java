package com.legacy.premium_wood.world.tree;

import com.legacy.premium_wood.registry.PWRegistry;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.world.level.LevelSimulatedReader;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FoliagePlacerType;

public class WeepingFoliagePlacer extends BlobFoliagePlacer
{
	public static final MapCodec<WeepingFoliagePlacer> WILLOW_CODEC = RecordCodecBuilder.mapCodec((instance) -> blobParts(instance).apply(instance, WeepingFoliagePlacer::new));

	public WeepingFoliagePlacer(IntProvider spread1, IntProvider spread2, int heightIn)
	{
		super(spread1, spread2, heightIn);
	}

	@Override
	protected FoliagePlacerType<?> type()
	{
		return PWRegistry.WILLOW.get();
	}

	@Override
	protected void createFoliage(LevelSimulatedReader worldIn, FoliagePlacer.FoliageSetter blocks, RandomSource randIn, TreeConfiguration configIn, int p_161364_, FoliagePlacer.FoliageAttachment foliageIn, int maxSize, int p_161367_, int getSize)
	{
		for (int currentY = getSize; currentY >= getSize - maxSize; --currentY)
		{
			// these two lines are copied from vanilla
			int width = Math.max(p_161367_ + foliageIn.radiusOffset() - 1 - currentY / 2, 0);
			this.placeLeavesRow(worldIn, blocks, randIn, configIn, foliageIn.pos(), width, currentY, foliageIn.doubleTrunk());
			int weepingWidth = 3; // The width the weeping leaves will place at.

			if (width >= weepingWidth)
			{
				for (int x = -width; x <= width; ++x)
				{
					for (int z = -width; z <= width; ++z)
					{
						// Checking to make sure the width is proper
						if ((x >= weepingWidth || x <= -weepingWidth || z >= weepingWidth || z <= -weepingWidth) && !(Math.abs(x) == width && Math.abs(x) == Math.abs(z)))
						{
							// The chance that a weeping stalk will place
							if (randIn.nextFloat() < 0.20F)
							{
								// place three leaves in the stalk
								for (int y = 0; y > -3; y--)
								{
									// the height that the weeping stalks will place (starting from the top)
									int startingWeepHeight = getSize - maxSize - 1 + y;
									BlockPos.MutableBlockPos blockpos$mutable = new BlockPos.MutableBlockPos();
									// setting the position
									blockpos$mutable.setWithOffset(foliageIn.pos(), x, startingWeepHeight, z);

									// actually placing the leaves
									/*if (TreeFeature.validTreePos(worldIn, blockpos$mutable))*/
									tryPlaceLeaf(worldIn, blocks, randIn, configIn, blockpos$mutable);
								}
							}
						}
					}
				}

			}
		}
	}

	// height
	@Override
	public int foliageHeight(RandomSource randIn, int p_230374_2_, TreeConfiguration configIn)
	{
		return super.foliageHeight(randIn, p_230374_2_, configIn);
	}

	@Override
	protected boolean shouldSkipLocation(RandomSource p_230373_1_, int p_230373_2_, int p_230373_3_, int p_230373_4_, int p_230373_5_, boolean p_230373_6_)
	{
		return p_230373_2_ == p_230373_5_ && p_230373_4_ == p_230373_5_ && (p_230373_1_.nextInt(2) == 0 || p_230373_3_ == 0);
	}
}