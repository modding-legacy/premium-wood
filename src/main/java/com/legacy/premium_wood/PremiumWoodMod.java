package com.legacy.premium_wood;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.premium_wood.client.PremiumEntityRendering;
import com.legacy.premium_wood.client.PremiumResourcePackHandler;
import com.legacy.premium_wood.item.ToolCompat;
import com.legacy.premium_wood.registry.PWBlockSets;
import com.legacy.premium_wood.registry.PWCreativeTabs;
import com.legacy.premium_wood.registry.PWWoodTypes;

import net.minecraft.client.renderer.Sheets;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.furnace.FurnaceFuelBurnTimeEvent;

@Mod(PremiumWoodMod.MODID)
public class PremiumWoodMod
{
	public static final String NAME = "Premium Wood";
	public static final String MODID = "premium_wood";

	public static final Logger LOGGER = LogManager.getLogger("ModdingLegacy/" + PremiumWoodMod.MODID);

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(PremiumWoodMod.MODID, name);
	}

	public static String find(String name)
	{
		return PremiumWoodMod.MODID + ":" + name;
	}

	public PremiumWoodMod(IEventBus modBus, ModContainer modContainer)
	{
		modContainer.registerConfig(ModConfig.Type.COMMON, PremiumConfig.COMMON_SPEC);

		IEventBus forgeBus = NeoForge.EVENT_BUS;

		modBus.addListener((FMLCommonSetupEvent event) ->
		{
			event.enqueueWork(() ->
			{
				ToolCompat.init();
				PWWoodTypes.init();
				PWBlockSets.init();
			});
		});

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			PremiumEntityRendering.init(modBus);

			modBus.addListener(PremiumResourcePackHandler::packRegistry);
			modBus.addListener(PremiumWoodMod::clientInit);
		}

		forgeBus.addListener((FurnaceFuelBurnTimeEvent event) -> FuelHandler.getBurnTime(event));
		forgeBus.register(PWEvents.class);

		modBus.addListener(PWCreativeTabs::init);
	}

	public static void clientInit(FMLClientSetupEvent event)
	{
		event.enqueueWork(() ->
		{
			Sheets.addWoodType(PWWoodTypes.MAPLE);
			Sheets.addWoodType(PWWoodTypes.TIGER);
			Sheets.addWoodType(PWWoodTypes.SILVERBELL);
			Sheets.addWoodType(PWWoodTypes.PURPLE_HEART);
			Sheets.addWoodType(PWWoodTypes.WILLOW);
			Sheets.addWoodType(PWWoodTypes.MAGIC);
		});
	}
}
